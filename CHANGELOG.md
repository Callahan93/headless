# Changelog

## v2.x

[**v2.0**](https://gitlab.com/Callahan93/headless/compare/v1.1...v2.0)

Finalised traffic light detection, lane detection & artificial neural network

## v1.x

[**v1.1**](https://gitlab.com/Callahan93/headless/compare/v1.0...v1.1)

Classifiers trained and tested (V15 is best), bug fixes and recording of RC remote state with lane detection info

[**v1.0**](https://gitlab.com/Callahan93/headless/compare/v0.7...v1.0)

Add image streaming up to 30ffps via TCP for data capture & traffic lights complete

## v0.x

[**v0.7**](https://gitlab.com/Callahan93/headless/compare/v0.6...v0.7)

Auto tuning and testing of image classifiers and update to classifier documentation with support tools

[**v0.6**](https://gitlab.com/Callahan93/headless/compare/v0.5...v0.6)

Add support for classifier training and testing

[**v0.5**](https://gitlab.com/Callahan93/headless/compare/v0.4...v0.5)

Add more tools: split_training.py, check_annotations.py. Update to Doxygen and GitLab CI. Auto build docs with version numbers

[**v0.4**](https://gitlab.com/Callahan93/headless/compare/v0.3...v0.4)

Add more tools: resize_all, video2img. Update pylint and Doxygen

[**v0.3**](https://gitlab.com/Callahan93/headless/compare/v0.2...v0.3)

Fix auto documentation and add PDF manual, first tool added = rename_all

[**v0.2**](https://gitlab.com/Callahan93/headless/compare/v0.1...v0.2)

Documentation on pages and automatic syntax checking of code 

[**v0.1**](https://gitlab.com/Callahan93/headless/compare/v0.0...v0.1)

Automatic documentation via doxygen with some example and testing files

**v0.0**

Initial release includes: README, CHANGELOG, docs/LOGO

