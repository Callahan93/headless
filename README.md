# Headless  
## README  
\image html 4773687.png
\image latex 4773687.eps "headless - 4773687" width=10cm

Docs are hosted on [gitlab.io](https://callahan93.gitlab.io/headless/) Using Doxygen and Doxypypy. 

But you can also download the source documents from this project's pipeline [here](https://gitlab.com/Callahan93/headless/-/jobs/artifacts/master/download?job=doxygen_manual).
The manual should also be available in PDF form [here](https://gitlab.com/Callahan93/headless/-/jobs/artifacts/master/download?job=doxygen_pdf).

Or try from [here](https://gitlab.com/Callahan93/headless/pipelines) if the above links do not work.

Some images may not render correctly on GitLab due to this project being coded for Doxygen.

Copyright © 2018 Matt Callahan