"""
Artificial Neural Network Training

This module will train an ANN based on the inputted files using OpenCV3(.3).
"""

import time
import numpy as np
import cv2
import matplotlib.pyplot as plt
#import pandas as pd
from sklearn.metrics import confusion_matrix


def main_function(npz_file='./comb_np_data1523714305.npz'):
    """
        This is the main function.

        This function is the main function (if using this file standalone).
        It first shuffles the input data whilst balancing before using OpenCV3 to train the ANN.
        Once trained it then saves and reloads the model before testing it (confusion matrix) on the same training data
        to be used as an indication only.

        Kwargs:
            npz_file: the numpy file containing the input and output data.
        """

    # Load training data
    #nn_ins = np.zeros((1, 2400), dtype=np.float32)  # 32float gives full res from 16int
    #nn_outs = np.zeros((1, 4), dtype=np.float32)     # float (use as confidence)
    print('Loading file...', end='')
    with np.load(npz_file) as data:
        print('Done')
        print(data.files)
        nn_ins = data['nn_ins']
        nn_outs = data['nn_outs']
        print(nn_ins.shape)
        print(nn_outs.shape)
        #print(np.delete(nn_outs, 1, 1).shape)   # delete backwards

    combined = np.hstack((nn_ins, nn_outs))
    orig_dat_len = combined.shape[0]
    print('Shuffling Data...', end='')
    np.random.seed(93)  # Choose a random seed to make this program repeatable
    np.random.shuffle(combined)  # Shuffle the array now that they are partnered with expected outs/ins
    print('Done')
    print('Balancing Data...')
    totals = np.sum(combined, axis=0)  # Re-evaluate
    out_totals = np.array([totals[-4], totals[-2], totals[-1]])     # Remove BWD from balanceing (we are not really going to be doing BWD atm)
    print(out_totals)

    rem_sum = out_totals[np.argmax(out_totals)] - out_totals[np.argmin(out_totals)]
    print('Need to remove {} data entries'.format(rem_sum))

    complex_flag = False
    count = 0
    tol = int(out_totals[np.argmin(out_totals)] * 0.02)
    print(tol)
    #rem_sum = 0
    while rem_sum >= tol:   # out_totals ignore BWD, do while in-balanced
        count += 1
        print('\nThis data is inbalanced, balancing now...')
        print(out_totals)

        sums = np.sum(combined[:, -4:], axis=1)     # Make an array of sums (identify combined vs simple out commands)

        place_max = (np.argmax(out_totals) - 4)  # Find the index for the combined array
        if place_max >= -3:  # Ignore BWD
            place_max += 1  # Compensate for no BWD if its LFT or RIT [FWD, BWD, LFT, RIT]
        place_min = (np.argmin(out_totals) - 4)
        if place_min >= -3:
            place_min += 1

        list_to_remove = []
        #complex_flag = True
        for idx, each_row in enumerate(combined):   # Run through each row of combined with row id
            if sums[idx] <= 1 and each_row[place_max] == 1:     # If simple command and the right command (likley FWD)
                list_to_remove.append(idx)      # Add to the list
            elif complex_flag and each_row[place_max] == 1:  # If any combination so long as it has the one we want)
                list_to_remove.append(idx)  # Add to the list

        rem_sum = out_totals[np.argmax(out_totals)] - out_totals[np.argmin(out_totals)]
        #print('Need to remove {} data entries to get to {} tolerance'.format(rem_sum, tol))
        if len(list_to_remove) < rem_sum:   # If the simple list is larger then its easy otherwise get the rest next round
            complex_flag = True
        else:
            complex_flag = False
            diff = int(len(list_to_remove) - rem_sum)
            del list_to_remove[:diff]   # Cut the list down
        #print('Removing {} data entries...'.format(int(len(list_to_remove))), end='')

        list_to_remove = sorted(list_to_remove, reverse=True)  # Arange largest first (to stop messing up indexes when deleting)
        combined = np.delete(combined, list_to_remove, axis=0)  # Delete them from the list

        #print('Shuffling Data...', end='')
        np.random.shuffle(combined)  # Shuffle the array now that they are partnered with expected outs/ins
        #print('Done')
        totals = np.sum(combined, axis=0)  # Re-evaluate
        out_totals = np.array([totals[-4], totals[-2], totals[-1]])
        print('Balanced to {}'.format(rem_sum - tol))
        print(out_totals)

        if count == 20:     # Break out if in an endless loop
            break

    print('Ran {} times!'.format(count))
    print('Data is now balanced with {} entries from {}!'.format(combined.shape[0], orig_dat_len))

    nn_ins = combined[:, :-4].astype(np.float32)    # Save the balanced array (must use as type for OpenCV)
    nn_outs = combined[:, -4:].astype(np.float32)
    print('Saving balanced data for next time')
    np.savez('./bala_np_data{}.npz'.format(npz_file[-14:-4]), nn_ins=nn_ins, nn_outs=nn_outs)  # Save balanced data as numpy file

    t_0 = time.time()    # Set start time

    # create MLP
    #layer_sizes = np.int32([2400, 32, 4])
    #model = cv2.ANN_MLP()
    #model.create(layer_sizes)
    #criteria = (cv2.TERM_CRITERIA_COUNT | cv2.TERM_CRITERIA_EPS, 500, 0.0001)
    #criteria2 = (cv2.TERM_CRITERIA_COUNT, 100, 0.001)
    #params = dict(term_crit=criteria,
    #              train_method=cv2.ANN_MLP_TRAIN_PARAMS_BACKPROP,
    #              bp_dw_scale=0.001,
    #              bp_moment_scale=0.0)

    # declare the nn
    ann = cv2.ml.ANN_MLP_create()
    #ann.setLayerSizes(np.array([2400, 160, 80, 4], dtype=np.uint16))
    ann.setLayerSizes(np.array([2400, 16, 4], dtype=np.uint16))
    print(ann.getLayerSizes())
    ann.setActivationFunction(cv2.ml.ANN_MLP_SIGMOID_SYM, 2, 1)
    #ann.setActivationFunction(cv2.ml.ANN_MLP_GAUSSIAN, 2, 1)
    ann.setTrainMethod(cv2.ml.ANN_MLP_BACKPROP)
    #ann.setTrainMethod(cv2.ml.ANN_MLP_RPROP)
    # EPS only at 10% (as we have low sample numbers)
    ann.setTermCriteria((2, 1000, 0.1))     # Experimental, default should be 3, 1000, 0.01 but does not give same result
    print(ann.getTrainMethod())
    print('ANN created')

    print('Training ANN ...')
    #num_iter = model.train(nn_ins, nn_outs, None, params=params)
    num_iter = ann.train(nn_ins, cv2.ml.ROW_SAMPLE, nn_outs)

    t_1 = time.time()     # Set end time
    print('Training Complete!')

    tot_sec = (t_1 - t_0)
    tot_min = divmod(tot_sec, 60)[0]
    tot_hr = divmod(tot_min, 60)[0]
    tot_min -= (60 * tot_hr)
    tot_sec -= ((60 * tot_min) + (3600 * tot_hr))
    print('Training ran {} iterations in Hr:{} Mn:{} S:{}'.format(num_iter, tot_hr, tot_min, tot_sec))

    print('Saving model...', end='')
    ann.save('./ann-{}.xml'.format(npz_file[-14:-4]))   # Save param
    print('Saved')

    print('Loading model...', end='')
    ann = cv2.ml.ANN_MLP_load('./ann-{}.xml'.format(npz_file[-14:-4]))
    print('Done')

    print('Testing model...')
    ret, resp = ann.predict(nn_ins)
    print(resp)
    #resp[resp <= 0.1] = 0   # threshold these (make a choice)
    #resp[resp > 0.1] = 1
    #print(resp)

    # https://stackoverflow.com/questions/46953967/multilabel-indicator-is-not-supported-for-confusion-matrix
    results = confusion_matrix(nn_outs.argmax(axis=1), resp.argmax(axis=1))
    #results = confusion_matrix(nn_outs, resp)

    # normalise
    results = results.astype('float') / results.sum(axis=1)[:, np.newaxis]  # http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html
    print(results)
    print('\n\n')

    # https://stackoverflow.com/questions/40729875/calculate-precision-and-recall-in-a-confusion-matrix?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
    true_pos = np.diag(results)
    false_pos = np.subtract(np.sum(results, axis=0), true_pos)
    false_neg = np.subtract(np.sum(results, axis=1), true_pos)
    precision = np.divide(true_pos, np.add(true_pos, false_pos))
    recall = np.divide(true_pos, np.add(true_pos, false_neg))
    results = np.hstack((results, precision.reshape((results.shape[0], 1))))    # Reshape Prec to fit it to CM
    results = np.vstack((results, np.hstack((recall, 0.0))))        # Add duff number to make CM square again
    print(results)

    labels = ['FWD', 'BWD', 'LFT', 'RIT']
    fig = plt.figure()
    ax = fig.add_subplot(111)
    cax = ax.matshow(results, cmap='PRGn')
    plt.title('Confusion Matrix - {}'.format(npz_file[-14:-4]))
    fig.colorbar(cax)
    ax.set_xticklabels([''] + labels + ['PREC'])
    ax.set_yticklabels([''] + labels + ['REC'])
    plt.xlabel('Predicted')
    plt.ylabel('Truth')
    # https://stackoverflow.com/questions/20998083/show-the-values-in-the-grid-using-matplotlib?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
    for (i, j), z in np.ndenumerate(results):
        ax.text(j, i, '{:0.3f}'.format(z), ha='center', va='center', bbox=dict(boxstyle='round', facecolor='white', edgecolor='0.3'))

    plt.savefig('Confusion Matrix - {}'.format(npz_file[-14:-4]), bbox_inches='tight')
    plt.show()
    #time.sleep(5.0)
    plt.close(fig)

    return


if __name__ == "__main__":
    print('Hello')
    print('This is \'train_ANN.py\'\nSTART:')
    main_function()
    print('End Program')
    exit(0)
