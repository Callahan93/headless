## Classifier README  

# Step 1

* Collect image/video data, where videos are used to quickly gather data make sure to split it down into frames 
with the tools/video2img.py  --> params are asked in the CLI step by step.
* This set up has only been tested with jpg & mp4 files but should work with others.
* Were possible, record data in the environment it is intended to be used in.

# Step 2

* Manually sort the images into positive and negative folders
* Delete images where the object is half in/out as this could cause confusion on the pos or neg data
* [Optionally] rename all the images to a common format to avoid large file names and confusion. Use the tools/rename_all.py
* Resize all the images to a common size (take care not to distort the image) - This makes training simpler, use the smallest resolution possible whilst the object is still clearly shown.
Use the tools/resize_all.py

# Step 3

* [Optionally] play with the positive dataset with image effects to get the most clarity on the object.
* Its advisable to set the image to grayscale to increase speed (only evaluates on colour rather than all 3 BGR). (resize_all.py can also convert to grayscale at the same time).
* Set all images to this pre-process format once decided

# Step 4

* Annotate the positive dataset with all the detections (try to use the same bounding box ratio)
* When making annotations with OpenCV's builtin tool, use folder path in the CLI and launch from the '/tools/' folder 
to match up file paths with the classifier testing output file.

opencv_annotation -images <folder location> -annotations <output file>

opencv_annotation -images ../bin/ -annotations ./annotations.txt

* Remember to save the file to the tools folder! (relative paths are used in all the python tools).

# Step 5

* [Optionally] move the annotations file into the positive dataset folder.
* Run the tools/split_training.py to create seperat training and testing folders etc.
* Note that the split training function also splits the annotations file but keeps the same relative link
so move the training set to a temp area (suggest ../bin/) before starting the OpenCV training process.

# Step 6

* Run the OpenCV training function - for help on parameters see the OpenCV3 Blueprints book and other resources.
* Once the classifier has been trained save it in the ../trained_classifiers/ area.

# Step 7

* Validate the classifier with tools/test_classifier.py to prove its working or not, you may need to adjust some of the tuning alues (accessable from the CLI through optional arguments).
Make sure to run it on the validation set (same images as training but less of them), [Optionally] run it on the full training set.
* Once working, run the auto tuning function from tools/tune_detections.py (use the -h) to find potential top parameters.
* [Optionally] re-run this tuning with the argument -m f which could take over an hour but will go through the same process in finer steps.
* Now we have proven it test it by re running against the test dataset (these images are not used in training). This helps to avoid over fitting.

# Step 8

* To improve the classifier further, crop out all the incorrect detections and feed it back into the negative training set but be sure to rename it such that it is at the top of the list.
* Then re-run the previous step
