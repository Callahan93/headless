"""
Check Annotations File

This module can analise the annotation files as well as perform precision recall on the given files.
Use '-h' for usage.
"""

from os import path     # To check the path is correct before opening
import argparse     # For the args (auto tuning etc.)
import numpy as np  # Use Numpy for inter-quartile range

# pylint: disable=R0914
# Too many local variables

class AnnotateFile(object):
    """
    This is class AnnotateFile.

    This class is 'AnnotateFile' is used to perform analysis on the annotations file used by OpenCV3.
    This is useful when setting training parameters for image classifiers.
    """

    def __init__(self, annotate_path):
        """
            The initiator for AnnotateFile class.

            This function is '__init__' and is used to initiate this class.
            This class is designed to be able to analise data from the annotations file when creating image classifiers.

            Args:
                self:           This function is part of the class 'AnnotateFile'.
                annotate_path:  This is the file path that will be used for this class

            Returns:
                An object.

            Examples:
                file_path = '../posann.txt'
                posann = AnnotateFile(file_path)
            """

        with open(annotate_path, mode='r') as file:
            self.lines = [line.rstrip('\n') for line in file]  # Remove line feeds and put into array
            self.img_total = len(self.lines)  # How many lines in file = images/files total
            # print(self.img_total)

    def data_split(self, line_number):
        """
            Will split the data on a given line number.

            This function is 'data_split' and is used to split the string data(with a space ' ') into separate useful data
            for any given line number in the annotations file. Currently only used internally by 'detections'.

            Args:
                self:           This function is part of the class 'AnnotateFile'.
                line_number:    This is the line number that the function will analise.

            Returns:
                A single list of separated words to be later processed.

            Examples:
                img_data = self.data_split(img_file)  # Get the data from the line
            """
        data_line = self.lines[line_number]  # Only look at one line
        words = []
        last = 0
        for pos, char in enumerate(data_line):  # Split the line into char
            if char == ' ':  # Wait till new word
                words.append(data_line[last:pos])  # Store the word
                last = pos+1  # Skip the space
            elif pos+1 == len(data_line) and last != pos+1:  # Or if this is the last word (no ' ' at the end)
                words.append(data_line[last:])  # Store the word
        return words

    def detections(self, img_file, data_x=True, data_y=True, data_w=True, data_h=True, data_s=False):
        """
            Find total number of detections for a given line number.

            This function is 'detections' and is used to find the total number of detections
            for a given line number. Currently only used internally by 'total_xywh'.

            Args:
                self:       This function is part of the class 'AnnotateFile'.
                img_file:   This is the line number that the function will analise.

            Kwargs:
                data_x:     If true will return x cord data else empty. default: True
                data_y:     If true will return y cord data else empty. default: True
                data_w:     If true will return width data else empty. default: True
                data_h:     If true will return height data else empty. default: True
                data_s:     If true will return detection score data else empty. default: False.
                            Note that this requires file format (file, num, x, y, w, h, scr)

            Returns:
                Number of detections, x cords, y cords, widths, heights, scores

            Examples:
                    my_tot, my_x, my_y, my_w, my_h, my_s = self.detections(each_line)
            """

        # Pass in file number (line number form pos.txt)
        img_data = self.data_split(img_file)  # Get the data from the line
        # print(img_data)
        retval_x = []
        retval_y = []
        retval_w = []
        retval_h = []
        retval_s = []

        if data_s and int(img_data[1]):   # Only use this set if looking at a detections file [file, num, x, y, w, h, scr]
            if data_x:
                for each in img_data[2::5]:
                    retval_x.append(int(each))
            if data_y:
                for each in img_data[3::5]:
                    retval_y.append(int(each))
            if data_w:
                for each in img_data[4::5]:
                    retval_w.append(int(each))
            if data_h:
                for each in img_data[5::5]:
                    retval_h.append(int(each))
            if data_s:
                for each in img_data[6::5]:
                    retval_s.append(int(each))

        elif int(img_data[1]):  # Number of detections >0:
            if data_x:
                for each in img_data[2::4]:
                    retval_x.append(int(each))
            if data_y:
                for each in img_data[3::4]:
                    retval_y.append(int(each))
            if data_w:
                for each in img_data[4::4]:
                    retval_w.append(int(each))
            if data_h:
                for each in img_data[5::4]:
                    retval_h.append(int(each))

        return int(img_data[1]), retval_x, retval_y, retval_w, retval_h, retval_s

    def total_xywh(self):
        """
            Retrieves x, y, w, h data from the annotations file.

            This function is 'total_xywh' and is used to gather all the x cord, y cord, width, height data
            from the annotations file. This function is part of the class 'AnnotateFile'

            Args:
                self:   This function is part of the class 'AnnotateFile'.

            Returns:
                A list for each set of data (x, y, w, h)

            Examples:
                ret_x, ret_y, all_width, all_height = posann.total_xywh()
            """

        all_x = []
        all_y = []
        all_w = []
        all_h = []

        for each_line in range(self.img_total):  # Run through every line
            my_tot, my_x, my_y, my_w, my_h, temp_s = self.detections(each_line)
            if my_tot > 0:
                for every_detect in range(my_tot):
                    all_x.append(my_x[every_detect])
                    all_y.append(my_y[every_detect])
                    all_w.append(my_w[every_detect])
                    all_h.append(my_h[every_detect])
        return all_x, all_y, all_w, all_h

    def find_line_number(self, name):
        """
            Finds the data on the requested line number.

            This function is 'find_line_number' and is used to find the line number that the image 'name'
            is currently specified on inside the annotations file. This function is part of the class 'AnnotateFile'

            Args:
                self:   This function is part of the class 'AnnotateFile'
                name:   Specify the file name (string) to search for.

            Returns:
                Returns the line number only

            Examples:
                num = posann.find_line_number('cool_image')
                print(num)
            """

        my_line = [each_line for each_line in range(self.img_total) if self.data_split(each_line)[0] == name]   # Look for first exact match first
        if not my_line:
            print('WARNING: Using wildards to find first result')
            my_line = [each_line for each_line in range(self.img_total) if name in self.data_split(each_line)[0]]   # Else search with wildcard
        if not my_line:
            return None   # Return an error if no result found
        #else:
        return my_line[0]  # Return first found instance of 'filename'


def rect_overlap_basic(ref_rect, test_rect, size_simil=0.2, overlap_simil=0.5, use_size=False):
    """
        Determine if 2 rectangles are similar and overlap.

        This function is 'rect_overlap_basic' and is used to find if two rectangles are similar and overlap.
        The rectangles are intended to come from an OpenCV object detection in the format [x, y, w, h].
        Idea is taken from [tom10 (Stackoverflow, 2014)]
        (https://stackoverflow.com/questions/27152904/calculate-overlapped-area-between-two-rectangles).

        Args:
            ref_rect:   Specify the reference rectangle (in OpenCV format (x,y,w,h) where we are operating in the 4th quadrant (bottom right)).
                        This reference is what the other rectangle will be judged against.
            test_rect:  Specify the second rectangle (in OpenCV format (x,y,w,h) where we are operating in the 4th quadrant (bottom right)).

        Kwargs:
            size_simil:     The size similarity percentage (100% = 1) required to pass. default: 0.2
            overlap_simil:  The overlap similarity percentage (100% = 1) required to pass. default: 0.5
            use_size:       Pass only if they are similar size as well if true. default: False

        Returns:
            A true or false if it passes the criteria.

        Examples:
            my_w = 4
            my_h = 3
            body1 = (10, 10, 5, 2)
            body2 = (11, 9, 5, 4)
            over = rect_overlap_basic(body1, body2)
            print(over)
            'True'
    """

    area_ref = ref_rect[2] * ref_rect[3]  # Width * Height
    area_test = test_rect[2] * test_rect[3]

    delt_x = min(ref_rect[0] + ref_rect[2], test_rect[0] + test_rect[2]) - max(ref_rect[0], test_rect[0])
    delt_y = min(ref_rect[1] + ref_rect[3], test_rect[1] + test_rect[3]) - max(ref_rect[1], test_rect[1])
    if (delt_x >= 0) and (delt_y >= 0):
        area_overlap = delt_x * delt_y

        if area_ref * (1 - overlap_simil) <= area_overlap <= area_ref * (1 + overlap_simil):
            if area_ref * (1 - size_simil) <= area_test <= area_ref * (1 + size_simil) and use_size:
                return True     # Similar sizes and overlap
            elif use_size:
                return False    # Similar overlap but not similar size, return false if required
            return True         # Similar overlap, similar size is not required
    return False    # No overlap (dont care about size now)


def rect_overlap_find(rect1, rect2, canvas_w, canvas_h, ref_rect1=True, thresh_per=50, extra=False):
    """
        Find the area of two overlapping rectangles.

        This function is 'rect_overlap_find' and is used to find the area of overlap between two rectangles.
        The rectangles are intended to come from an OpenCV object detection.
        This over lap can be used for confidence and merging detections.
        Idea is taken from [Perrone, R (Stackoverflow, 2014)]
        (https://stackoverflow.com/questions/244452/what-is-an-efficient-algorithm-to-find-area-of-overlapping-rectangles).

        Args:
            rect1:   Specify the first rectangle (in OpenCV format (x,y,w,h) where we are operating in the 4th quadrant (bottom right)).
            rect2:   Specify the second rectangle (in OpenCV format (x,y,w,h) where we are operating in the 4th quadrant (bottom right)).
            canvas_w:   The width of the canvas (whole image), of which the rectangle is in. Note: both rectangles must be on the same canvas.
            canvas_h:   The height of the canvas (whole image), of which the rectangle is in. Note: both rectangles must be on the same canvas.

        Kwargs:
            ref_rect1:      The refrence rectangle for percentage overlap. default: True (rect1 is reference)
            thresh_per:     The percentage barrier for overlapping pass. default: 50(%)
            extra:          Return all info (more variables returned) if true. default: False

        Returns:
            A true or false if it passes the criteria.
            Else (if extra = True) will return the above plus 'overlap %', 'area of intersect', 'area of union'.

        Examples:
                my_w = 4
                my_h = 3
                body1 = (2, 0, 2, 2)
                body2 = (1, 1, 2, 2)
                over = rect_overlap_find(body1, body2, my_w, my_h, thresh_per=25)
                print(over)
                'True'
        """

    x = 0   # Define OpenCV detection rectangle, positions
    y = 1
    w = 2
    h = 3

    rect_a = np.zeros((canvas_h, canvas_w))     # Init the arrays
    rect_b = np.zeros((canvas_h, canvas_w))

    rect_a[rect1[y]:(rect1[y] + rect1[h]), rect1[x]:(rect1[x] + rect1[w])] = 1
    rect_b[rect2[y]:(rect2[y] + rect2[h]), rect2[x]:(rect2[x] + rect2[w])] = 1

    area_union = np.sum((rect_a + rect_b) > 0)      # 0+1 = 1
    area_intersect = np.sum((rect_a + rect_b) > 1)  # 1+1 = 2

    if ref_rect1:
        overlap_per = int((area_intersect * 100) / (np.sum(rect_a)))    # Return a value of 0-100%
    else:
        overlap_per = int((area_intersect * 100) / (np.sum(rect_b)))

    if not extra:
        return thresh_per <= overlap_per    # Return true or false if meets criteria
    return (thresh_per <= overlap_per), overlap_per, area_intersect, area_union     # If extra info is asked for then return all info


def get_iqr_avg(my_array, top=80, bottom=20, stats=False):
    """
        Get the inter quartile range average.

        This function is 'get_iqr_avg' and is used to find the inter quartile range of a list and then average it
        which is useful for statistics starting object training parameters.

        Args:
            my_array:   Specify the array too analise.

        Kwargs:
            top:        The percentage barrier for upper quartile. default: 80
            bottom:     The percentage barrier for lower quartile. default: 20
            stats:      If stats is true it will return the barrier values as well. default: False

        Returns:
            If stats is true then it will return a tupple: mean, iqr_mean, [qmax, qmid, qmin].
            Else it will return the average and inter average: mean, iqr_mean

        Examples:
            height_avg, height_iqr, height_stats = get_iqr_avg(all_height, stats=True)
            print(' WIDTH = AVG: {:>6.2f}, IQR_AVG: {:>6.2f}, MAX: {:>6.2f}, MID: {:>6.2f}, MIN: {:>6.2f}'
            .format(width_avg, width_iqr, width_stats[0], width_stats[1], width_stats[2]))
        """

    my_array_float = np.array(my_array, dtype='float32')

    qmax, qhi, qmid, qlo, qmin = np.percentile(my_array_float, [100, top, 50, bottom, 0])  # Get the top and bottom percent value
    print('MAX|-{}%-|MID|-{}%-|MIN\n{}|--{}---|{}|---{}--|{}'.format(top, bottom, qmax, qhi, qmid, qlo, qmin))
    iqr_float = np.array([x for x in my_array_float if qlo > x < qhi], dtype='float32')     # Make a filtered list based on percent values
    # print('Old Range:\n{}'.format(my_array_float))
    # print('New Range:\n{}'.format(iqr_float))
    iqr_mean = np.mean(iqr_float)
    mean = np.mean(my_array_float)

    if stats:
        return mean, iqr_mean, [qmax, qmid, qmin]
    #else:
    return mean, iqr_mean


def compare_files(ann_path, det_path, score_thresh=0):
    """
        Compare 2 OpenCV annotation files for classifier performance.

        This function is 'compare_files' and is used to compare 2 OpenCV annotation files.
        Both files should have the same images in the same order.

        Args:
            ann_path:   Specify the ground truth file as outputted by OpenCV annotations tool.
            det_path:   Specify the detections file as outputted by the custom 'test_classifier' tool.

        Kwargs:
            score_thresh:   This is the score threshold an image must reach in order to be counted. default: 0

        Returns:
            Precision & recall values for given results. Prec=TP/(TP+FP) Recl=TP/(TP+FN)

        Examples:
            >>> dat_prec, dat_recl = compare_files(gt_path, dt_path, score_thresh=thresh)
            '0.8571	0.4286'
    """

    gt_file = AnnotateFile(ann_path)
    dt_file = AnnotateFile(det_path)
    TP = 0
    TN = 0
    FP = 0
    FN = 0

    for each_img_file in range(gt_file.img_total):  # Run through every line

        #gt_rect = np.empty([1, 4], dtype=int)
        gt_det_num, gt_cord_x, gt_cord_y, gt_box_w, gt_box_h, gt_score = gt_file.detections(each_img_file)  # Ground truth
        gt_rect = np.hstack((
            np.reshape(gt_cord_x, (len(gt_cord_x), 1)),
            np.reshape(gt_cord_y, (len(gt_cord_y), 1)),
            np.reshape(gt_box_w, (len(gt_box_w), 1)),
            np.reshape(gt_box_h, (len(gt_box_h), 1))))

        dt_det_num, dt_cord_x, dt_cord_y, dt_box_w, dt_box_h, dt_score = dt_file.detections(each_img_file, data_s=True)  # Detections
        dt_rect = np.hstack((       # List into [n,1] arrays then horizontally stack = array of rectangles
            np.reshape(dt_cord_x, (len(dt_cord_x), 1)),
            np.reshape(dt_cord_y, (len(dt_cord_y), 1)),
            np.reshape(dt_box_w, (len(dt_box_w), 1)),
            np.reshape(dt_box_h, (len(dt_box_h), 1))))

        for count, elem in enumerate(dt_score):     # Remove all detections that don't meet the score threshold
            if elem <= score_thresh:
                dt_rect = np.delete(dt_rect, count, axis=0)  # Remove the rect that passed
                del dt_score[count]  # Remove the score from the list
                dt_det_num -= 1  # Reduce count

        if gt_det_num >= 1 and dt_det_num >= 1:
            gt_pass_cnt = 0
            for each_gt_detection in range(gt_det_num):     # Run through each detection
                if dt_det_num > 0:  # Don't test if we have ran out of detections
                    for each_dt_detection in range(dt_det_num):     # Run through every detection on each GT detection
                        if gt_det_num >= 1 and dt_det_num >= 1:   # Ensure correct indexing
                            match = rect_overlap_basic(gt_rect[each_gt_detection, ], dt_rect[each_dt_detection, ])     # Compare the 2 detections
                        elif gt_det_num >= 1:
                            match = rect_overlap_basic(gt_rect[each_gt_detection, ], dt_rect)
                        elif dt_det_num >= 1:
                            match = rect_overlap_basic(gt_rect, dt_rect[each_dt_detection, ])
                        else:
                            match = rect_overlap_basic(gt_rect, dt_rect)

                        # If match, record index row number here
                # Remove recorded index from list
                        if match:
                            TP += 1     # Record result
                            dt_rect = np.delete(dt_rect, each_dt_detection, axis=0)  # Remove the rect that passed
                            del dt_score[each_dt_detection]  # Remove the score from the list
                            dt_det_num -= 1  # Reduce count
                            gt_pass_cnt += 1    # Keep track of how many GT passed
                            break   # Break out from testing each DT (carry on testing next GT)
                # <<<<<<---------
            FP += dt_det_num    # Leftover detections are false
            FN += (gt_det_num - gt_pass_cnt)   # Leftover ground truths are FN

        elif gt_det_num >= 1 and dt_det_num == 0:
            #print('FN found')
            FN += gt_det_num

        elif gt_det_num == 0 and dt_det_num >= 1:
            #print('FP found')
            FP += dt_det_num

        elif gt_det_num == 0 and dt_det_num == 0:
            #print('Pass')
            TN += 1
    prec = 1    # Division by zero tends to infinity = 1
    recl = 1
    if TP == 0:     # TP is on top so it will always = 0
        if FN > 0:
            recl = 0    # Else it is set to 1 (above) (division by zero)
        if FP > 0:
            prec = 0
    else:       # Normal numbers so calculate normally
        recl = TP / (TP + FN)
        prec = TP / (TP + FP)

    return prec, recl


def get_prec_recl(gt_path, dt_path, thresh_min=0, thresh_max=80, thresh_step=1, res_folder='./', name='1', AUC=False):
    """
            Iterates multiple classifier tests to get best score (threshold).

            This function is 'get_prec_recl' and is used to generate prec, recl data including thresholds.
            From this, the best threshold can then be selected

            Args:
                gt_path:    Specify ground truth file with path.
                dt_path:    Specify detections file with path.

            Kwargs:
                thresh_min:        Will output a txt file like OpenCV for results [file, amount, x_n, y_n, w_n, h_n, score_n : False
                                Saved in img_folder.
                thresh_max:     The max threshold level to test. default: 30
                thresh_step:    The min threshold level to test. default: 30
                res_folder:     The folder to output the data to. default: './'
                name:           The name (post-pend) for the data file. default: '1'
                AUC:            If True it wil calculate the Area Under the Curve (trapiziod)
                                as an indication of performance. defualt False

            Returns:
                P, R, T data and saves this to a .txt file specified by 'name' param. Will return AUC (not P, R, T) if True.

            Examples:
                >>> get_prec_recl('./annotations.txt', './detections.txt')
            """

    data_points = np.empty([1, 3], dtype=float)  # Initiate an array to accumulate results
    thresh = thresh_min
    while True:
        if thresh < thresh_max:
            dat_prec, dat_recl = compare_files(gt_path, dt_path, score_thresh=thresh)
        else:
            break
        thresh += thresh_step
        data_points = np.vstack((data_points, [dat_prec, dat_recl, thresh]))

    data_points = np.delete(data_points, 0, 0)  # Delete original top row
    if AUC:
        # Compute the area using the composite trapezoidal rule.
        area = np.trapz(data_points[:, 0], data_points[:, 1])
        # Consider scipi with simpsons rule (more accurate - samples should be odd)
        #print("area =", area)
        return 0 - area


    np.savetxt('{}prec_recl_{}.txt'.format(res_folder, name), data_points, delimiter=',', fmt='%.4f')  # Write array result to file
    return data_points



def main_function():
    """
        This is the main function.

        This function is the main function (if using this file standalone).
        It runs get_iqr_avg function with values inputted by the user - each parameter is asked for step by step

        Examples:
            Please enter file path to the annotations.tx
            >>>../posann.txt
            '...'
        """

    file_path = input("Please enter file path to the annotations.txt\n>>>")
    if path.isfile(file_path):
        print('INFO: Path to  file \'{}\''.format(file_path))
    else:
        print('Could not find file \'{}\'')
        print('WARNING: Using default file path \'../posann.txt\'')
        file_path = '../posann.txt'
        if path.isfile(file_path):
            print('file \'{}\' found')
        else:
            print('ERROR: Default folder was not found!')
            return  # Break out

    posann = AnnotateFile(file_path)
    my_file = posann.detections(0)
    print(my_file)
    print(posann.img_total)
    # pylint: disable=W0612
    ret_x, ret_y, all_width, all_height = posann.total_xywh()
    width_avg, width_iqr, width_stats = get_iqr_avg(all_width, stats=True)
    height_avg, height_iqr, height_stats = get_iqr_avg(all_height, stats=True)
    print('***RESULTS***')
    print(' WIDTH = AVG: {:>6.2f}, IQR_AVG: {:>6.2f}, MAX: {:>6.2f}, MID: {:>6.2f}, MIN: {:>6.2f}'
          .format(width_avg, width_iqr, width_stats[0], width_stats[1], width_stats[2]))
    print('HEIGHT = AVG: {:>6.2f}, IQR_AVG: {:>6.2f}, MAX: {:>6.2f}, MID: {:>6.2f}, MIN: {:>6.2f}'
          .format(height_avg, height_iqr, height_stats[0], height_stats[1], height_stats[2]))

    num = posann.find_line_number('35')
    print(num)
    if num is not None:
        my_file = posann.detections(num)
        print(my_file)
    else:
        print('Not found')

    return

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-pr', '--precRecl', nargs='?',
                        help='s = get IQRavg from file, else do a PR curve', default='s')
    parser.add_argument('-gt', '--gtFile', nargs='?',
                        help='the ground truth file (OpenCV format)', default='./annotations.txt')
    parser.add_argument('-dt', '--dtFile', nargs='?',
                        help='the detection results file (like OpenCV foramt)', default='./detections.txt')

    # Process args
    args = parser.parse_args()

    my_gt_file = args.gtFile
    my_dt_file = args.dtFile

    print('Hello')
    print('This is \'check_annotations.py\'\nSTART:')
    if args.precRecl == 's':
        main_function()
    else:
        ret = get_prec_recl(my_gt_file, my_dt_file)
        print(ret)
    print('End Program')

    exit(0)
