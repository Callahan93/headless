"""
Rename All

This module renames all images to a given format and will ask for parameters through the terminal.
"""

import os  # For file operations


def rename_img(input_folder, output_folder, dry_run=False, only_extensions=['.jpg', '.png'], prefix='n_'):
    """
        Rename multiple images.

        This function is 'rename_img' and is used to rename multiple image files to a fixed format
        which is useful for image organisation.

        Args:
            input_folder:   Specify the folder too look for images (will not search recursively).
            output_folder:  This is where the renamed images will be saved.

        Kwargs:
            dry_run:            If true will print results but will not action it. default: False
            only_extensions:    Will search files only with extensions specified. default: ['.jpg', '.png']
            prefix:             Will put this string at the beginning of every file. default: 'n_'

        Returns:
            Nothing, it will print all results and failed files when running.

        Examples:
            >>> rename_img('../bin', '../', prefix='TEST_', dry_run=True)
            'DRY RUN:'
            'Found 4 files in ../bin/'
            'DRY:Renamed 0.jpg to TEST_000000.jpg'
            'DRY:Renamed 1.jpg to TEST_000001.jpg'
            'DRY:Renamed 2.jpg to TEST_000002.jpg'
            'DRY:Renamed 3.jpg to TEST_000003.jpg'
        """

    # pylint: disable=W0102
    # Dangerous default value [] as argument (dangerous-default-value)

    files = [f for f in sorted(os.listdir(input_folder)) if f[-4:].lower() in only_extensions]
    no_of_files = len(files)
    print('Found {} files in {}'.format(no_of_files, input_folder))

    failed_files = []
    for (index, filename) in enumerate(files):  # Run through each file on the list

        # extension = os.path.splitext(filename)[1]  # Extension is not always 4 characters
        extension = filename[-4:].lower()
        new_name = ("{}{:0>6d}{}".format(prefix, index, extension.lower()))

        if os.path.exists(os.path.join(output_folder, new_name)):  # If the file already exists then find a new number
            print('WARNING: File \'{}{}\' already exists!'.format(output_folder, new_name))
            failed_files.append(filename)
        else:
            if dry_run:
                print("DRY:Renamed {} to {}".format(filename, new_name))
            else:
                os.rename(os.path.join(input_folder, filename), os.path.join(output_folder, new_name))
                print("Renamed {} to {}".format(filename, new_name))

    if failed_files:  # Try process again with the failed files and new numbers
        max_number = no_of_files  # Start enumerating from the largest file number
        for (index, filename) in enumerate(files):
            extension = filename[-4:].lower()
            new_name = ("{}{:0>6d}{}".format(prefix, int(index+max_number), extension.lower()))
            if os.path.exists(os.path.join(output_folder, new_name)):  # If the file already exists then give up
                print('ERROR: File \'{}{}\' already exists\nABORTING FILE!'.format(output_folder, new_name))
                # Continue to next file and try that
            else:
                if dry_run:
                    print("DRY:Renamed {} to {}".format(filename, new_name))
                else:
                    os.rename(os.path.join(input_folder, filename), os.path.join(output_folder, new_name))
                    print("Renamed {} to {}".format(filename, new_name))

    return


def main_function():
    """
        This is the main function.

        This function is the main function (if using this file standalone).
        It runs rename_img function with values inputted by the user - each parameter is asked for step by step

        Examples:
            Please enter folder path to image files
            >>>../bin/
            INFO: Path to image files '../bin/'
            INFO: Acceptable image formats are ['.jpg', '.png', '.tiff']
            Please enter the output folder
            >>>../
            INFO: Output folder is '../'
            Please enter a prefix
            >>>TEST_
            Would you like to operate as a dry run?
            >>>y
        """

    img_formats = ['.jpg', '.png', '.tiff']

    folder = input("Please enter folder path to image files\n>>>")
    if os.path.isdir(folder) and folder[-1] == '/':
        print('INFO: Path to image files \'{}\''.format(folder))
        print('INFO: Acceptable image formats are {}'.format(img_formats))
    else:
        print('Could not get folder \'{}\'\n'
              'Please check syntax eg: \'../bin/\' not \'../bin\''.format(folder))
        print('WARNING: Using default path \'../bin/\'')
        folder = '../bin/'
        if os.path.isdir(folder):
            print('INFO: Acceptable image formats are {}'.format(img_formats))
        else:
            print('ERROR: Default folder was not found!')
            return  # Break out

    out_folder = input("Please enter the output folder\n>>>")  # Decide output path
    if os.path.isdir(out_folder) and folder[-1] == '/':
        print('INFO: Output folder is \'{}\''.format(out_folder))
    elif os.path.isdir(out_folder):
        print('Please check syntax eg: \'../bin/\' not \'../bin\'\n'
              'Will use  folder \'{}/\''.format(out_folder))
        out_folder('{}/'.format(out_folder))
    else:
        print('WARNING: Can not find folder \'{}\''.format(out_folder))
        yes_no = input('Would you like to create this folder?\n>>>').lower()
        if yes_no == 'yes' or yes_no == 'y':
            if out_folder[-1] == '/':
                print('Creating directory \'{}\''.format(out_folder))
                os.mkdir(out_folder)
            else:
                print('Creating directory \'{}/\''.format(out_folder))
                os.mkdir('{}/'.format(out_folder))
                out_folder = ('{}/'.format(out_folder))
        else:
            print('WARNING: Using default output path \'../bin/\'')
            out_folder = '../bin/'

    prefix = str(input("Please enter a prefix\n>>>")) # Decide prefix (as string)
    if prefix == '':
        print('WARNING: No prefix chosen, using default \'n_\'')
        prefix = 'n_'

    yes_no = input('Would you like to operate as a dry run?\n>>>').lower()
    if yes_no == 'yes' or yes_no == 'y':
        print('DRY RUN:')
        rename_img(folder, out_folder, only_extensions=img_formats, prefix=prefix, dry_run=True)
    else:
        rename_img(folder, out_folder, only_extensions=img_formats, prefix=prefix)

    return


if __name__ == "__main__":
    print('Hello')
    print('This is \'rename_me.py\'\nSTART:')
    main_function()
    print('End Program')
    exit(0)
