"""
Resize All

This module resizes all images to a given format and will ask for parameters through the terminal.
"""

import os  # For file operations
import cv2 	# For image manipulation


def resize_img(input_folder, output_folder, dry_run=False, only_extensions=['.jpg', '.png'], prefix='n_',
               rs_width=320, rs_height=240, allow_crop=True):
    """
        Resize multiple images.

        This function is 'resize_img' and is used to resize multiple image files to a fixed size and format
        which is useful for image preparation for classifier training.

        Args:
            input_folder:   Specify the folder too look for images (will not search recursively).
            output_folder:  This is where the renamed images will be saved.

        Kwargs:
            dry_run:            If true will print results but will not action it. default: False
            only_extensions:    Will search files only with extensions specified. default: ['.jpg', '.png']
            prefix:             Will put this string at the beginning of every file. default: 'n_'
            rs_width:			The resized width (in pixels) default 320
            rs_height:			The resized height (in pixels). default 240
            allow_crop:			Will crop the image if the new ratio does not math the original. default: True

        Returns:
            Nothing, it will print all results and failed files when running.

        Examples:
            'Hello'
            'Please enter folder path to image files'
            '>>>../pos/'
            INFO: Path to image files '../pos/'
            INFO: Acceptable image formats are ['.jpg', '.png']
            '...'
            'DRY RUN:'
            'Resizing 0.jpg from 240x320 to 240x320 into ../bin/'
            'Resizing 1.jpg from 240x320 to 240x320 into ../bin/'
            'Resizing 10.jpg from 240x320 to 240x320 into ../bin/'

    """

    files = [f for f in sorted(os.listdir(input_folder)) if f[-4:].lower() in only_extensions]

    print('Resizing images...')
    for image_file in files:
        try:
            #img = cv2.imread(folder + str(image) + '.jpg')  # Open image (first is 0.jpg) (opened with OpenCV)
            img = cv2.imread('{}{}'.format(input_folder, image_file))
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)  # Convert to gray scale

            # Keep smallest dimension larger than our resized dimension
            if img.shape[0] >= img.shape[1]:  # Portrait
                ratio = float(rs_width) / img.shape[0]  # [0]=height, [1]=width
                dim = (int(rs_height), int(img.shape[0] * ratio))  # Resize in same aspect ratio
            else:  # Assume height < width (landscape)
                ratio = float(rs_width) / img.shape[0]  # [0]=height, [1]=width
                dim = (int(img.shape[1] * ratio), int(rs_width))  # Resize in same aspect ratio

            resized_img = cv2.resize(img, dim)  # We resize the image to height x width

            # Check from crop allow here
            if (resized_img.shape[0] != rs_height) or (resized_img.shape[1] != rs_width) and allow_crop:
                # print('{} x {}'.format(resized_img.shape[0], resized_img.shape[1]))
                crop_img = resized_img[
                           int(resized_img.shape[0] / 2) - int(rs_height / 2):
                           int(resized_img.shape[0] / 2) + int(rs_height / 2),
                           int(resized_img.shape[1] / 2) - int(rs_width / 2):
                           int(resized_img.shape[1] / 2) + int(rs_width / 2)]  # Crop to center
                # img[y: y+h, x: x+w]
                # img[y1:y2, x1,x2] where (x1,y1) is top left of crop and (x2,y2) is bottom right of crop)
                print('CROPPED: ', end='')
            else:
                crop_img = resized_img

            if dry_run:  # Print info
                print('DRY: ', end='')

            print('\r\033[;0m', end='')  # For progress bar only (mac/linux)
            print('Resizing {} from {}x{} to {}x{} into {}'.format(image_file,
                                                                   img.shape[0], img.shape[1],
                                                                   crop_img.shape[0], crop_img.shape[1],
                                                                   # img.shape[0], img.shape[1], rs_height, rs_width,
                                                                   output_folder))
            if not dry_run:  # Write image only if this is not a dry run
                #cv2.imwrite(output_folder + str(pic_num) + '.jpg', crop_img)  # Save the result in the "small" folder
                cv2.imwrite('{}{}{}'.format(output_folder, prefix, image_file), crop_img)

        except Exception as Error:  # If there was a problem during the operation
            # pylint: disable=W0703
            # Intentionally catch all errors
            print(cv2.getBuildInformation())  # Double check the build state
            print(str(Error))  # Print the exception
            print(image_file)
            print(img)  # Make sure image is not corrupt or empty
            exit(10)  # Stop at first failure

    print('\033[;0mFinished! :)\nAssumed BGR colour format of original images (OpenCV standard)')
    return


def main_function():
    """
        This is the main function.

        This function is the main function (if using this file standalone).
        It runs resize_img function with values inputted by the user - each parameter is asked for step by step

        Examples:
            Please enter folder path to image files
            '>>>../bin/'
            INFO: Path to image files '../bin/'
            INFO: Acceptable image formats are ['.jpg', '.png', '.tiff']
            Please enter the output folder
            '>>>../'
            INFO: Output folder is '../'
            Please enter a prefix
            '>>>TEST_'
            Would you like to operate as a dry run?
            '>>>y'
        """

    img_formats = ['.jpg', '.png', '.tiff']

    folder = input("Please enter folder path to image files\n>>>")  # Decide input Path
    if os.path.isdir(folder) and folder[-1] == '/':
        print('INFO: Path to image files \'{}\''.format(folder))
        print('INFO: Acceptable image formats are {}'.format(img_formats))
    elif os.path.isdir(folder):
        print('Please check syntax eg: \'../bin/\' not \'../bin\'')
        folder = '{}/'.format(folder)
        print('WARNING: Using path \'{}\''.format(folder))

    else:
        print('WARNING: Could not get folder \'{}\''.format(folder))
        print('WARNING: Using default path \'../bin/\'')
        folder = '../bin/'
        if os.path.isdir(folder):
            print('INFO: Acceptable image formats are {}'.format(img_formats))
        else:
            print('ERROR: Default folder was not found!')

    out_folder = input("Please enter the output folder\n>>>")  # Decide output path
    if os.path.isdir(out_folder) and folder[-1] == '/':
        print('INFO: Output folder is \'{}\''.format(out_folder))
    elif os.path.isdir(out_folder):
        print('Please check syntax eg: \'../bin/\' not \'../bin\'\n'
              'Will use  folder \'{}/\''.format(out_folder))
        out_folder('{}/'.format(out_folder))
    else:
        print('WARNING: Can not find folder \'{}\''.format(out_folder))
        yes_no = input('Would you like to create this folder?\n>>>').lower()
        if yes_no == 'yes' or yes_no == 'y':
            if out_folder[-1] == '/':
                print('Creating directory \'{}\''.format(out_folder))
                os.mkdir(out_folder)
            else:
                print('Creating directory \'{}/\''.format(out_folder))
                os.mkdir('{}/'.format(out_folder))
                out_folder = ('{}/'.format(out_folder))
        else:
            print('WARNING: Using default output path \'../bin/small/\'')
            out_folder = '../bin/small/'
            if not os.path.exists(out_folder):  # Does the folder "small" exists ?
                print('Creating directory {}'.format(out_folder))
                os.makedirs(out_folder)  # If not, create it for our results
            elif os.listdir(out_folder):  # If folder exists and is not empty
                yes_no = input('Files exist in \'{}\' already!\n'
                               'Would you like to delete the contents?\n>>>'.format(out_folder)).lower()
                if yes_no == 'yes' or yes_no == 'y':
                    print('Deleting files...')
                    delete_files = [f for f in os.listdir(out_folder)]  # List all files in folder
                    for each in delete_files:
                        os.remove('{}{}'.format(out_folder, each))
                        print('.', end='')
                    print('Done')
                else:
                    print('Leaving existing files alone')
            #else:  # Folder exists and is empty
                #None

    prefix = str(input("Please enter a prefix\n>>>"))  # Decide prefix (as string)
    if prefix == '':
        print('WARNING: No prefix chosen, using default \'n_\'')
        prefix = 'n_'

    new_width = input("Please enter a new width\n>>>")  # Decide width (as int)
    if new_width == '':
        print('WARNING: No width chosen, using default \'320\'')
        new_width = 320
    else:
        new_width = int(new_width)
        print('INFO: Chosen width = {}'.format(new_width))

    new_height = input("Please enter a new height\n>>>")  # Decide width (as int)
    if new_height == '':
        print('WARNING: No height chosen, using default \'240\'')
        new_height = 240
    else:
        new_height = int(new_height)
        print('INFO: Chosen height = {}'.format(new_height))

        # Do you want to allow cropping if the original and new ratio is different?
    yes_no = input('Do you want to allow cropping if the original and new ratio is different?\n>>>').lower()
    if yes_no == 'yes' or yes_no == 'y':
        print('INFO: Allow Cropping')
        crop_allow = True
    else:
        print('INFO: Disable Cropping')
        crop_allow = False

    yes_no = input('Would you like to operate as a dry run?\n>>>').lower()
    if yes_no == 'yes' or yes_no == 'y':
        print('DRY RUN:')
        resize_img(folder, out_folder, rs_width=new_width, rs_height=new_height, prefix=prefix, allow_crop=crop_allow,
                   dry_run=True)
    else:
        print('START:')
        resize_img(folder, out_folder, rs_width=new_width, rs_height=new_height, prefix=prefix, allow_crop=crop_allow)

    return


if __name__ == '__main__':

    print('Hello')
    print('This is \'rename_me.py\'\nSTART:')
    main_function()
    print('End Program')
    exit(0)
