"""
Split Training Data

This module will split a given data-set into training, validation & testing and will ask for parameters through the terminal.
"""

import os  # For file operations
from random import sample   # For random number generation
import shutil       # For copying of files
# pylint: disable=R0914
# Too many local variables


def split_data(input_folder, output_folder, only_extensions=['.jpg', '.png'], train_set=80, validate_set=40,
               remove=False, data_move=False, data_file=''):
    """
        Randomly split data in proportional percentages.

        This function is 'data_split' and is used to split an image data set to a fixed format
        which is useful for image organisation.

        Args:
            input_folder:   Specify the folder too look for images (will not search recursively).
            output_folder:  This is where the organised data set will be saved (folders: test, train, validate).

        Kwargs:
            only_extensions:    Will search files only with extensions specified. default: ['.jpg', '.png']
            train_set:          This is the percentage of the whole set to be used for training. default: 80
            validate_set:       This is the percentage of the whole training set to be used for validation. default: 40
            test_set:           This automatic (not setable by the user) and is = 100% - train set. default: 20
            remove:             Optional delete of original files. default: False
            data_move:          If Tru it will also split the master annotations file to move with testing/validation. defualt: False

        Returns:
            Nothing, it will print all results and failed files when running.

        Examples:
            >>> split_data(input_folder, output_folder, train_set=80, validate_set=40)
            'Found 10 files in ../bin/'
            'Creating dir: ../split/test'
            'Creating dir: ../split/train'
            'Creating dir: ../split/validate'
            'Moving Files now ...'
            'Done'
        """
    # pylint: disable=W0102
    # Dangerous default value [] as argument (dangerous-default-value)

    files = [f for f in sorted(os.listdir(input_folder)) if f[-4:].lower() in only_extensions]
    no_of_files = len(files)
    print('Found {} files in {}'.format(no_of_files, input_folder))

    test_no_indexs = sample(range(0, no_of_files), int(round((no_of_files / 100) * (100 - train_set), 0)))   # Store the random indexes
    test_list = []
    for every_random in test_no_indexs:
        test_list.append(files[every_random])     # Grab every random index file
    train_list = [e for e in files if e not in test_list]

    no_of_files = len(train_list)    # Update the list
    validate_no_indexs = sample(range(0, no_of_files), int(round((no_of_files / 100) * validate_set, 0)))   # Store the random indexes
    validate_list = []
    for every_random in validate_no_indexs:
        validate_list.append(train_list[every_random])     # Grab every random index file

    if os.path.isdir('{}test'.format(output_folder)):  # Make the folder if doesn't already exist
        shutil.rmtree('{}test'.format(output_folder))  # start with a fresh folder
        print('Removing existing contents: {}test'.format(output_folder))
    print('Creating dir: {}test'.format(output_folder))
    os.mkdir('{}test'.format(output_folder))
    if os.path.isdir('{}train'.format(output_folder)):  # Make the folder if doesn't already exist
        shutil.rmtree('{}train'.format(output_folder))  # start with a fresh folder
        print('Removing existing contents: {}train'.format(output_folder))
    os.mkdir('{}train'.format(output_folder))
    print('Creating dir: {}train'.format(output_folder))
    if os.path.isdir('{}validate'.format(output_folder)):  # Make the folder if doesn't already exist
        shutil.rmtree('{}validate'.format(output_folder))  # start with a fresh folder
        print('Removing existing contents: {}validate'.format(output_folder))
    os.mkdir('{}validate'.format(output_folder))
    print('Creating dir: {}validate'.format(output_folder))

    test_list = sorted(test_list)   # OpenCV likes the annotations file in sorted format
    train_list = sorted(train_list)
    validate_list = sorted(validate_list)

    split_this_file = ''
    if data_move and (data_file == ''):   # Assume the only txt file is the one we want
        data_file_opt = [f for f in sorted(os.listdir(input_folder)) if f[-4:].lower() in '.txt']
        if len(data_file_opt) > 1:
            print('Could not confirm data_file, found: {}'.format(data_file_opt))
            exit(0)
        print('Found file: {}'.format(data_file_opt))
        split_this_file = data_file_opt
    elif data_move:
        split_this_file = data_file
    if data_move and split_this_file:
        import check_annotations as ca    # Locally import the check annotations file to work out split points
        origin_file = ca.AnnotateFile(split_this_file)

        with open('{}test/annotations.txt'.format(output_folder), mode='w') as new_f:
            for each_item in test_list:
                new_f.write('{}\n'.format(origin_file.lines[origin_file.find_line_number('{}{}'.format(input_folder, each_item))]))
        with open('{}train/annotations.txt'.format(output_folder), mode='w') as new_f:
            for each_item in train_list:
                new_f.write('{}\n'.format(origin_file.lines[origin_file.find_line_number('{}{}'.format(input_folder, each_item))]))
        with open('{}validate/annotations.txt'.format(output_folder), mode='w') as new_f:
            for each_item in validate_list:
                new_f.write('{}\n'.format(origin_file.lines[origin_file.find_line_number('{}{}'.format(input_folder, each_item))]))
    else:
        print('No annotations file found!')

    print('Moving Files now ...')
    for each_file in test_list:
        shutil.copy2('{}{}'.format(input_folder, each_file), '{}test/'.format(output_folder))
    for each_file in train_list:
        shutil.copy2('{}{}'.format(input_folder, each_file), '{}train/'.format(output_folder))
    for each_file in validate_list:
        shutil.copy2('{}{}'.format(input_folder, each_file), '{}validate/'.format(output_folder))

    if remove:
        print('Removing original images...')
        for every_file in files:
            os.remove(os.path.join(input_folder, every_file))
    print('Done')
    return


def main_function():
    """
        This is the main function.

        This function is the main function (if using this file standalone).
        It runs split_data function with values inputted by the user - each parameter is asked for step by step

        Examples:
            Please enter folder path to data set
            >>>../bin/
            INFO: Path to data set '../bin/'
            INFO: Acceptable image formats are ['.jpg', '.png']
            Please enter the output folder
            >>>../split/
            INFO: Output folder is '../split'
            Should we delete the original files after arranging them?
            >>>n
            OK - keeping originals...
            Found 10 files in ../bin/
            Creating dir: ../split/test
            Creating dir: ../split/train
            Creating dir: ../split/validate
            Moving Files now ...
            Done
            End Program
        """

    img_formats = ['.jpg', '.png', '.tiff']

    folder = input("Please enter folder path to data set\n>>>")
    if os.path.isdir(folder) and folder[-1] == '/':
        print('INFO: Path to data set \'{}\''.format(folder))
        print('INFO: Acceptable image formats are {}'.format(img_formats))
    else:
        print('Could not get folder \'{}\'\n'
              'Please check syntax eg: \'../bin/\' not \'../bin\''.format(folder))
        print('WARNING: Using default path \'../bin/\'')
        folder = '../bin/'
        if os.path.isdir(folder):
            print('INFO: Acceptable image formats are {}'.format(img_formats))
        else:
            print('ERROR: Default folder was not found!')
            return  # Break out

    out_folder = input("Please enter the output folder\n"
                       "we will create sub directories under this\n>>>")  # Decide output path
    if os.path.isdir(out_folder) and folder[-1] == '/':
        print('INFO: Output folder is \'{}\''.format(out_folder))
        print('WARNING: MAKE SURE THAT outfolder/test, train, validate/ FOLDERS ARE EMPTY OR HAVE NONE OF THE SAME IMAGES IN')
    elif os.path.isdir(out_folder):
        print('Please check syntax eg: \'../split/\' not \'../split\'\n'
              'Will use  folder \'{}/\''.format(out_folder))
        out_folder('{}/'.format(out_folder))
        print('WARNING: MAKE SURE THAT outfolder/test, train, validate/ FOLDERS ARE EMPTY OR HAVE NONE OF THE SAME IMAGES IN')
    else:
        print('WARNING: Can not find folder \'{}\''.format(out_folder))
        yes_no = input('Would you like to create this folder?\n>>>').lower()
        if yes_no == 'yes' or yes_no == 'y':
            if out_folder[-1] == '/':
                print('Creating directory \'{}\''.format(out_folder))
                os.mkdir(out_folder)
            else:
                print('Creating directory \'{}/\''.format(out_folder))
                os.mkdir('{}/'.format(out_folder))
                out_folder = ('{}/'.format(out_folder))
        else:
            print('WARNING: Using default output path \'../split/\'')
            out_folder = '../split/'
            if not os.path.isdir(out_folder):   # Make the folder if doesn't already exist
                os.mkdir(out_folder)
            else:
                print('WARNING: MAKE SURE THAT outfolder/test, train, validate/ FOLDERS ARE EMPTY OR HAVE NONE OF THE SAME IMAGES IN')
    should_we = input("Should we delete the original files after arranging them?\n>>>").lower()
    if should_we == 'yes' or should_we == 'y':
        print('OK - we will delete originals...')
        should_we = True
    else:
        print('OK - keeping originals...')
        should_we = False

    should_we2 = input("Should we move the annotations file too?\n>>>").lower()
    if should_we2 == 'yes' or should_we2 == 'y':
        print('OK - True')
        the_anno_file = input('Enter the annotations file with path')
        if not the_anno_file:
            the_anno_file = './annotations.txt'
        should_we2 = True
        print('{} selected'.format(the_anno_file))
    else:
        print('OK - False')
        should_we2 = False
        the_anno_file = ''

    split_data(folder, out_folder, only_extensions=['.jpg', '.png'], train_set=80, validate_set=40,
               remove=should_we, data_move=should_we2, data_file=the_anno_file)
    return


if __name__ == "__main__":
    print('Hello')
    print('This is \'split_training.py\'\nSTART:')
    main_function()
    print('End Program')
    exit(0)
