"""
Test Cascades Classifier

This module can operate a given object detector, also storing its results.
Use '-h' for usage.
"""

import os
import shutil
import argparse
import numpy as np
import cv2

# pylint: disable=R0913, R0914, W0105
# Too many arguments (11/10), Too many local variables, String statement has no effect

def detect_obj(img_files, img_folder, detector, out_dat=False, imgout_folder='', out_crp=True, spot=True,
               scale_fac=1.05, min_si=(50, 50), max_si=(1000, 1000), neighbors=0, grp_thresh=2, epsilon=0.3, name='detections'):
    """
        Detect objects with cascades classifier.

        This function is 'detect_obj' and is used to detect all objects in all given images
        and then output the result to file or to the return value.
        A confidence score is also given for every detection.
        Currently only using OpenCV detectMultiScale (detectMultiScale2 & 3 are not currently in use).
        REQUIRES: [OpenCV v3.0+](https://docs.opencv.org/3.0-beta/modules/objdetect/doc/cascade_classification.html),
        [Numpy](https://docs.scipy.org/doc/numpy/reference/), OS, Shutil.

        Args:
            img_files:  Specify the image(s) to work on.
            img_folder: Specify the folder the images are located.
            detector:   Specify the classifier to use in detections.

        Kwargs:
            out_det:        Will output a txt file like OpenCV for results [file, amount, x_n, y_n, w_n, h_n, score_n : False
                            Saved in img_folder.
            imgout_folder:  If specified other than '' the detections will be drawn over the image and saved here. default: ''
            scale_fac:      OpenCV detectMultiScale scaleFactor. default: 1.05
            min_si:         OpenCV detectMultiScale minSize. default: (50,50)
            max_si:         OpenCV detectMultiScale maxSize. default: (1000,1000)
            neighbors:      OpenCV detectMultiScale minNeighbors. default: 0
            grp_thresh:     OpenCV groupRectangles, min number of rect to pass as a group (detection). default: 2
            epsilon:        OpenCV groupRectangles, epsilon is the amount of similarity, (can be greater than 1). default: 0.9

        Returns:
            A numpy 2D array of all detections found [x, y, w, h, score]. Also optionally saves the images to file.

        Examples:
            >>> retval = detect_obj(pic_list, folder, my_haar_cascade, imgout_folder='../testing/out/')
            >>> print(retval)
            '[[120  41 127 138 409]'
             '[149  72 119 129 358]'
             '[ 89  61 122 132 197]'
             '[126 130  74  80  11]'
             '[139  95 104 113 277]'
             '[106  80 114 124 301]'
             '[150  70 118 128 386]'
             '[ 23  73  57  62   3]'
             '[156  77 106 115 177]'
             '[100  55 133 144  20]'
             '[  5  28  71  77   8]]'
        """

    if imgout_folder != '' and os.path.isdir(imgout_folder):
        print('WARNING: Deleting persistent directory **{}**'.format(imgout_folder))
        shutil.rmtree(imgout_folder)  # Remove any existing directory
        os.mkdir(imgout_folder)

    if out_dat:
        with open('{}{}.txt'.format('./', name), 'w') as outfile:
            outfile.write('')   # Clear file or make new file

    detector_cascade = cv2.CascadeClassifier(detector)
    all_detects = np.empty([1, 5], dtype=int)   # Initiate an array to accumilate results
    img_files = sorted(img_files)   # Sort images to the same as OpenCV
    for image in img_files:
        cv_img = cv2.imread('{}{}'.format(img_folder, image))  # Open image (should be grayscale)
        cv_img_detecs = detector_cascade.detectMultiScale(cv_img, scale_fac, neighbors, minSize=min_si, maxSize=max_si)  # Detect objects
        # cv_img_detecs, numbermerged = detector_cascade.detectMultiScale2(img, scaleFactor=1.5, minNeighbors=1)
        # cv_img_detecs, class_lvl, class_weight = detector_cascade.detectMultiScale3(img, scaleFactor=1.5, minNeighbors=0, outputRejectLevels=True)

        '''
                if info_on and out_img:
                    for (x, y, w, h) in cv_img_detecs:  # Draw all the detections
                        # score = 1000 * (lvl[minicount, 0] + weight[minicount, 0])
                        # cv_img = cv2.rectangle(cv_img,(x,y),(x+w,y+h),(255,0,0),2)  # Draw a rectangle around detection (BGR)
                        # print('.')
        '''
        obj_det, obj_scr = cv2.groupRectangles(np.array(cv_img_detecs).tolist(), grp_thresh, epsilon)
        cv_obj = np.hstack((obj_det, obj_scr))  # Conjoin the score to the detection [x, y, w, h, scr]

        if cv_obj.size:
            del_list = []
            crp_cnt = 0
            for (detx, dety, detw, deth, detscr) in cv_obj:  # Draw all the detections
                crop_img = cv_img[dety:dety + deth, detx:detx + detw]  # Where (x1,y1) is top left of crop and (x2,y2) is bottom right of crop)
                crp_cnt += 1
                if spot:
                    if not np.any(crop_img > 180):
                        del_list.append(crp_cnt - 1)
                        continue

                if imgout_folder != '':  # If a out folder has been specified then save images there
                    if out_crp:
                        cv2.imwrite('{}CRP_{}_{}'.format(imgout_folder, crp_cnt, image), crop_img)
                    cv_img = cv2.rectangle(cv_img, (detx, dety), (detx + detw, dety + deth), (0, 255, 0), 2)  # Draw a rectangle around detection
                    cv_img = cv2.putText(cv_img, '{}'.format(detscr), (detx, dety), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)

                    cv2.imwrite('{}R{}'.format(imgout_folder, image), cv_img)
                    print('{}R{}'.format(imgout_folder, image))

            for each in reversed(del_list):   # Remove all false predictions (based on post proc if bright light)
                cv_obj = np.delete(cv_obj, each, axis=0)
            all_detects = np.append(all_detects, cv_obj, axis=0)

        if out_dat:
            np.savetxt('{}tempArrDet{}.txt'.format('./', image), cv_obj, delimiter=' ', fmt='%d')  # Write array result to file temporaly
            with open('{}tempArrDet{}.txt'.format('./', image), 'r') as tmpfile:
                det_lines = [line.rstrip('\n') for line in tmpfile]  # Remove line feeds and put into array
                num_det = len(det_lines)
                det_lines = ' '.join(det_lines)  # Flatten the list (if more than 1 detections)
                with open('{}{}.txt'.format('./', name), 'a') as outfile:  # Append the master file
                    outfile.write('{}{} {} {}\n'.format(img_folder, image, num_det, det_lines))
            os.remove('{}tempArrDet{}.txt'.format('./', image))  # Delete the temporary file

    all_detects = np.delete(all_detects, 0, 0)  # Delete original top row

    return all_detects


def main_function():
    """
        This is the main function.

        This function is the main function (if using this file standalone).
        It runs detect_obj function with default values. Detection values can be set via a CLI (use -h for help).

        Examples:
            'Hello'
            'This is 'test_classifier.py''
            'START:'
            'Found 13 test images in ../testing/'
            'Begin Testing...,
            'Found 11 detections in 13 files'
            'End Program'
        """

    parser = argparse.ArgumentParser()
    # Folder paths args
    parser.add_argument('-i', '--inputFolder', nargs='?',
                        help='Input folder path like \'../bin/\', must have / on the end', default='../bin/')
    parser.add_argument('-o', '--outputFolder', nargs='?',
                        help='Output folder path like \'../bin/out/\', must have / on the end', default='../bin/out/')
    parser.add_argument('-c', '--classifier', nargs='?',
                        help='classifier file path like \'../trained_classifiers/star1.xml\'', default='../trained_classifiers/star1.xml')
    parser.add_argument('-f', '--imgFormat', nargs='+',
                        help='Image extensions to use like \'.jpg .png\'', default='.jpg .png')
    parser.add_argument('-no', '--noImageOut', action='store_true', help='If specified then the image data will not be stored')

    # Tuning params args
    parser.add_argument('-mn', '--minNeighbors', nargs='?', help='OpenCV: minNeighbors', type=int, default=0)
    parser.add_argument('-sf', '--scaleFactor', nargs='?', help='OpenCV: scale Factor', type=float, default=1.05)
    parser.add_argument('-ms', '--minSize', nargs='+', help='OpenCV: minSize', type=int, default=[6, 14])
    parser.add_argument('-sm', '--maxSize', nargs='+', help='OpenCV: maxSize', type=int, default=[100, 240])

    # Process args
    args = parser.parse_args()

    folder = args.inputFolder
    pic_formats = args.imgFormat
    my_haar_cascade = args.classifier
    if args.noImageOut:
        out_res_folder = ''
    else:
        out_res_folder = args.outputFolder

    pic_list = [f for f in os.listdir(folder) if f[-4:].lower() in pic_formats]  # Files only (no paths)
    # pic_list = ['{}{}'.format(folder, f) for f in os.listdir(folder) if f[-4:].lower() in pic_formats]  # With path
    print('Found {} test images in {}\nBegin Testing...'.format(len(pic_list), folder))

    ret_det = detect_obj(pic_list, folder, my_haar_cascade, imgout_folder=out_res_folder, out_dat=True,
                         scale_fac=args.scaleFactor, neighbors=args.minNeighbors, min_si=tuple(args.minSize), max_si=tuple(args.maxSize))
    print('Found {} detections in {} files'.format(ret_det.shape[0], len(pic_list)))

    return


if __name__ == "__main__":
    print('Hello')
    print('This is \'test_classifier.py\'\nSTART:')
    main_function()
    print('End Program')
    exit(0)
