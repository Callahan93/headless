"""
Tune Detections

This module iterates a given classifier and dat-set over multiple detection parameters to find the best (10).
This is based on its performance (precision/recall) so it also plots and saves these results for later.
Use '-h' for usage.
"""

import os   # To check the path is correct before opening
import shutil   # Remove tree only required
import argparse
import check_annotations as ca      # Local module to compare results
import test_classifier as tclass    # Local module to run classifier
import matplotlib.pyplot as plt     # To output results to graph

# pylint: disable=R0914
# Too many local variables


def main_function():
    """
        This is the main function.

        This function is the main function (if using this file standalone).
        It accepts multiple arguments (use -h for info).
        It will then run multiple iterations of detection tuning parameters and finish with the top 10 results as files.

        Note:
            The best results are determined by the area under the PR curve set in 'check_annotations'.
            A larger area is not always the best but will be close. Advise is to check individual results as needed.

        Examples:
            'python3 tune_detection'
            'Hello'
            "This is 'tune_detection.py'"
            'START:'
            'Found 36 test images in ../bin/'
            'Begin Testing...'
            '(1:256)	SF:1.05	MN:0	EP:0.5	AUC:0.03432635974386987'
            '(2:256)	SF:1.05	MN:0	EP:0.7	AUC:0.043180740456809974'
            '...'
            '(256:256)	SF:1.8	MN:3	EP:1.1	AUC:0.0'
            '******TOP 10 DETECTION PARAMETERS******'
            'SF:1.8	MN:0	EP:0.9	AUC:0.4039578477078477'
            'SF:1.4	MN:0	EP:1.1	AUC:0.40340909090909094'

        """

    parser = argparse.ArgumentParser()
    # Folder paths args
    parser.add_argument('-i', '--inputFolder', nargs='?',
                        help='Input folder path like \'../bin/\', must have / on the end', default='../bin/')
    parser.add_argument('-c', '--classifier', nargs='?',
                        help='classifier file path like \'../trained_classifiers/star1.xml\'',
                        default='../trained_classifiers/star1.xml')
    parser.add_argument('-f', '--imgFormat', nargs='+',
                        help='Image extensions to use like \'.jpg .png\'', default='.jpg .png')
    parser.add_argument('-m', '--mode', nargs='?',
                        help='Coarse or fine (c or f). '
                             'Fine will run over many more points to judge classifier (but this is slower)', default='c')
    parser.add_argument('-gt', '--gtFile', nargs='?',
                        help='The ground truth file (annotations)', default='./annotations.txt')

    # Process args
    args = parser.parse_args()

    mode_of_op = args.mode
    folder = args.inputFolder
    pic_formats = args.imgFormat
    my_haar_cascade = args.classifier
    gt_file = args.gtFile

    minSize = (6, 14)
    maxSize = (50, 115)

    pic_list = [f for f in os.listdir(folder) if f[-4:].lower() in pic_formats]  # Files only (no paths)
    # pic_list = ['{}{}'.format(folder, f) for f in os.listdir(folder) if f[-4:].lower() in pic_formats]  # With path
    print('Found {} test images in {}\nBegin Testing...'.format(len(pic_list), folder))

    AUCS = []
    lSF = []        # Use separate lists as numpy expects single type of data
    lMN = []
    lEP = []
    count = 0

    if mode_of_op == 'c':
        for val1 in range(5, 85, 5):   # 1=0.01 (1.05 >> 1.80)
            SF = (1 + (val1 / 100))
            #print('*', end='')
            for MN in range(0, 2, 1):   # (0 >> 3)
                #print('^', end='')
                for eps in range(2, 11, 2):     # (0.5 >> 1.1)
                    EP = (eps / 10)
                    count += 1
                    #print('CNT:{}\tSF:{}\t\tMN:{}\tEP:{}'.format(count, SF, MN, EP))
                    #continue
                    ret_det = tclass.detect_obj(pic_list, folder, my_haar_cascade, out_dat=True,    # Put detections to file
                                                scale_fac=SF, neighbors=MN,
                                                min_si=minSize, max_si=maxSize,
                                                name='_SF{}MN{}EP{}'.format(SF, MN, EP),
                                                epsilon=EP)
                    ret_pr = ca.get_prec_recl(gt_file, './_SF{}MN{}EP{}.txt'.format(SF, MN, EP), AUC=True)  # Open the file and add to my list
                    AUCS.append(ret_pr)
                    lSF.append(SF)
                    lMN.append(MN)
                    lEP.append(EP)
                    os.remove('_SF{}MN{}EP{}.txt'.format(SF, MN, EP))   # Delete the file after we have used it
                    #print('.', end='')
                    print('({}:256)\tSF:{}\tMN:{}\tEP:{}\tAUC:{}'.format(count, SF, MN, EP, ret_pr))      # Progress

    elif mode_of_op == 'f':     # Loop over many iterations to find the best set of param.
        for val1 in range(1, 51, 2):   # 1=0.01 (1.01 >> 1.80)
            SF = (1 + (val1 / 100))
            #print('*', end='')
            for MN in range(0, 1, 1):   # (0 >> 8)
                #print('^', end='')
                for eps in range(1, 12, 1):     # (0.5 >> 1.5)
                    EP = (eps / 10)
                    count += 1
                    #print('CNT:{}\tSF:{}\t\tMN:{}\tEP:{}'.format(count, SF, MN, EP))
                    #continue
                    ret_det = tclass.detect_obj(pic_list, folder, my_haar_cascade, out_dat=True,
                                                scale_fac=SF, neighbors=MN,
                                                min_si=minSize, max_si=maxSize,
                                                name='_SF{}MN{}EP{}'.format(SF, MN, EP),
                                                epsilon=EP)
                    ret_pr = ca.get_prec_recl(gt_file, './_SF{}MN{}EP{}.txt'.format(SF, MN, EP), AUC=True)
                    AUCS.append(ret_pr)
                    lSF.append(SF)
                    lMN.append(MN)
                    lEP.append(EP)
                    os.remove('_SF{}MN{}EP{}.txt'.format(SF, MN, EP))   # Delete the file after we have used it
                    #print('.', end='')
                    print('({}:275)\tSF:{}\tMN:{}\tEP:{}\tAUC:{}'.format(count, SF, MN, EP, ret_pr))

    with open('auc.txt', 'w') as the_file:      # Save all AUC results against params in a text file
        the_file.write('scaleFactor,minNeighbors,Epsilon,AUC\n')
        for index, item in enumerate(AUCS):
            the_file.write('{},{},{},{}\n'.format(lSF[index], lMN[index], lEP[index], item))

    if not os.path.isdir('./top10/'):
        os.mkdir('./top10/')        # mkdir if it doesn't exist
    else:
        shutil.rmtree('./top10/')   # Else delete its contents first
        os.mkdir('./top10/')

    print('******TOP 10 DETECTION PARAMETERS******')
    tops = sorted(zip(AUCS, lSF, lMN, lEP), reverse=True)[:10]      # Return the top n of AUCS as tuples
    tp_AUCS, tp_SF, tp_MN, tp_EP = map(list, zip(*tops))        # Unpack it back to lists
    for tp_idx in range(len(tp_AUCS)):
        print('SF:{}\tMN:{}\tEP:{}\tAUC:{}'.format(tp_SF[tp_idx], tp_MN[tp_idx], tp_EP[tp_idx], tp_AUCS[tp_idx]))

        ret_det = tclass.detect_obj(pic_list, folder, my_haar_cascade, out_dat=True,         # Re-run the detector and save to file
                                    scale_fac=tp_SF[tp_idx], neighbors=tp_MN[tp_idx],
                                    min_si=minSize, max_si=maxSize,
                                    name='_SF{}MN{}EP{}'.format(tp_SF[tp_idx], tp_MN[tp_idx], tp_EP[tp_idx]),
                                    epsilon=tp_EP[tp_idx])

        pr_pnts = ca.get_prec_recl(gt_file, '_SF{}MN{}EP{}.txt'.format(tp_SF[tp_idx], tp_MN[tp_idx], tp_EP[tp_idx]),
                                   name='SF{}MN{}EP{}'.format(tp_SF[tp_idx], tp_MN[tp_idx], tp_EP[tp_idx]),
                                   thresh_max=200, res_folder='./top10/')        # Get prec, recl points for the top 10
        os.remove('_SF{}MN{}EP{}.txt'.format(tp_SF[tp_idx], tp_MN[tp_idx], tp_EP[tp_idx]))      # Remove the detections file after we done PR
        #newFunc(pr_pnts[:, 1], pr_pnts[:, 0])
        #print(pr_pnts[:, 1])

        # Build up plot points
        plt.plot(pr_pnts[:, 1], pr_pnts[:, 0], label='SF:{} MN:{} EP:{}'.format(tp_SF[tp_idx], tp_MN[tp_idx], tp_EP[tp_idx]))
    # Set plot options
    plt.title('Precision Recall\n{}'.format(my_haar_cascade))
    plt.legend(loc='lower right', shadow=True)
    plt.axis([0, 1, 0, 1.05])
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.savefig('./top10/PR_top_ten_param.png', bbox_inches='tight')
    plt.show()

    return


if __name__ == "__main__":

    print('Hello')
    print('This is \'tune_detection.py\'\nSTART:')
    main_function()
    print('End Program')

    exit(0)
