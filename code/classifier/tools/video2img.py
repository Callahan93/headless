"""
Video to Image

This module accepts a video and then proceds to split it into individual frames and will ask for parameters through the terminal.
"""

import os  # For file operations
from random import sample  # For pulling random frames
import cv2 	# For image manipulation


def video_to_jpg(video_file, output_dir, frames=[], prefix='_frm', breakout=2000):
    """
        Extract multiple frames from a video(s).

        This function is 'video_to_jpg' and is used to extract frames of a video into a JPEG image
        which is useful for image collection.

        Args:
            video_file:     Specify the video file too extract frames from.
            output_dir:     This is where the extracted images will be saved.

        Kwargs:
            frames:     This is the frames wanted out of the videos with the following modes. default: ALL\n
                        RANDOM: frame=['r', n] then take 'n' number of random frames\n
                        SPECIFIED: frame=[n, n, n, ...] take these specific frames\n
                        ALL: frames == [] (empty) then get all frames
            prefix:     Will put this string at the beginning of every file. default: '_frm'
            breakout:   This is the maximum number of frames to extract during 'ALL Frames' (e.g. stop if video is too long. default: 2000

        Returns:
            Nothing, it will print all results and failed files when running.

        Examples:
            'Converting video myvid.mp4 to frames...'
            'Retrieving 3 random frames from myvid.mp4...'
            'Saving frame 2 as _frmmyvid2 in ./...'
            'Saving frame 30 as _frmmyvid30 in ./...'
            'Saving frame 401 as _frmmyvid401 in ./...'
            'video_to_jpg ... Done'
    """

    print('Converting video \'{}\' to frames...'.format(video_file))
    filename = video_file.split('/')  # Split into just the file name
    filename = filename[-1].split('.')  # Remove the extension

    video = cv2.VideoCapture(video_file)  # Create video object
    total_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))  # Get the total frame count

    if not frames:  # frames == [] (empty) then get all frames
        count = 0
        not_finish, image = video.read()
        while not_finish and count <= breakout:  # As we didn't know how many frames, breakout if too many
            print('Saving frame {}/{} as \'{}{}_{}.jpg\' in {}'
                  .format(count, total_frames, prefix, filename[0], count, output_dir))
            cv2.imwrite('{}{}{}_{}.jpg'.format(output_dir, prefix, filename[0], count), image)
            count += 1  # Keep track
            not_finish, image = video.read()  # Read next frame
        if count <= breakout:
            print('Breakout condition met')

    elif frames[0] == 'r':  # frame=['r', n] then take 'n' number of random frames
        print('Retrieving {} random frames from {}...'.format(frames[1], total_frames))
        frames_number = (sample(range(0, total_frames), frames[1]))  # Make a list of unique random numbers

        for each_frame in frames_number:
            video.set(cv2.CAP_PROP_POS_FRAMES, each_frame)  # Set video pointer to this frame
            success, image = video.retrieve()  # Get that frame
            if success:
                print('Saving frame {} as \'{}{}_{}.jpg\' in {}'
                      .format(each_frame, prefix, filename[0], each_frame, output_dir))
                cv2.imwrite('{}{}{}_{}.jpg'.format(output_dir, prefix, filename[0], each_frame), image)  # Save the image
            else:
                print('WARNING: Failed to get frame {}'.format(each_frame))

    else:  # frame=[n, n, n, ...] take these specific frames
        frames_number = [frm for frm in frames if 0 <= frm < total_frames]  # Take out only numbers in video
        print('WARNING: frames {}/{} do not exist in {}'
              .format((list(set(frames)-set(frames_number))), total_frames, video_file))  # Show the failure frames

        for each_frame in frames_number:
            video.set(cv2.CAP_PROP_POS_FRAMES, each_frame)  # Set video pointer to this frame
            success, image = video.retrieve()  # Get that frame
            if success:
                print('Saving frame {} as \'{}{}_{}.jpg\' in {}'
                      .format(each_frame, prefix, filename[0], each_frame, output_dir))
                cv2.imwrite('{}{}_{}.jpg'.format(output_dir, prefix, each_frame), image)  # Save the image
            else:
                print('Failed to retrieve frame {}'.format(each_frame))

    print('video_to_jpg ... Done')
    video.release()  # Kill the video capture


def main_function():
    """
        This is the main function.

        This function is the main function (if using this file standalone).
        It runs video_to_jpg function with values inputted by the user - each parameter is asked for step by step

        Examples:
            'Please enter folder path to videos'
            '>>>../bin/''
            'INFO: Path to video(s) '../bin/''
            'INFO: Acceptable video formats are ['.mp4']'
            'Please enter the output folder'
            '>>>../''
            'INFO: Output folder is '../''
            '...'
    """
    vid_formats = ['.mp4']

    folder = input("Please enter folder path to videos\n>>>")
    if os.path.isdir(folder) and folder[-1] == '/':
        print('INFO: Path to video(s) \'{}\''.format(folder))
        print('INFO: Acceptable video formats are {}'.format(vid_formats))
    else:
        print('Could not get folder \'{}\'\n'
              'Please check syntax eg: \'../bin/\' not \'../bin\''.format(folder))
        print('WARNING: Using default path \'../bin/\'')
        folder = '../bin/'
        if os.path.isdir(folder):
            print('INFO: Acceptable video formats are {}'.format(vid_formats))
        else:
            print('ERROR: Default folder was not found!')
            return  # Break out

    out_folder = input("Please enter the output folder\n>>>")  # Decide output path
    if os.path.isdir(out_folder) and folder[-1] == '/':
        print('INFO: Output folder is \'{}\''.format(out_folder))
    elif os.path.isdir(out_folder):
        print('Please check syntax eg: \'../bin/\' not \'../bin\'\n'
              'Will use  folder \'{}/\''.format(folder))
        out_folder = ('{}/'.format(out_folder))
    else:
        print('WARNING: Can not find folder \'{}\''.format(out_folder))
        yes_no = input('Would you like to create this folder?\n>>>').lower()
        if yes_no == 'yes' or yes_no == 'y':
            if out_folder[-1] == '/':
                print('Creating directory \'{}\''.format(out_folder))
                os.mkdir(out_folder)
            else:
                print('Creating directory \'{}/\''.format(out_folder))
                os.mkdir('{}/'.format(out_folder))
                out_folder = ('{}/'.format(out_folder))
        else:
            print('WARNING: Using default output path \'../bin/\'')
            out_folder = '../bin/'

    # video_list = [f for f in os.listdir(folder) if f[-4:].lower() in vid_formats]  # Files only (no paths)
    video_list = ['{}{}'.format(folder, f) for f in os.listdir(folder) if f[-4:].lower() in vid_formats]  # With path
    if video_list:  # make sure there are videos in there
        print('We found these videos\n{}'.format(video_list))
        for each_video in video_list:  # print how many frames available for info

            print('Working on video \'{}\''.format(each_video))
            video = cv2.VideoCapture(each_video)  # Create video object
            print('INFO:{} has {} total frames'
                  .format(each_video, int(video.get(cv2.CAP_PROP_FRAME_COUNT))))

            grab_mode = input("Please select a grab mode\n"
                              "\'-a\': All frames\n"
                              "\'-r\': Random frames\n"
                              "\'-s\': Specific frames\n"
                              ">>>").lower()
            if grab_mode not in ['-a', '-r', '-s']:
                print('ERROR: No command found')
                return
            else:
                prefix = input("Frames will be saved with the names\n"
                               "\'prefix\'\'video file name\'00357.jpg, please enter a prefix (if any)\n"
                               ">>>").lower()

            if grab_mode == '-a':
                print('Running all frames')
                video_to_jpg(each_video, out_folder, [], prefix=prefix, breakout=5)

            elif grab_mode == '-r':
                how_many_random = int(input("How many random frames do you want?\n>>>"))
                print('Running {} random frames'.format(how_many_random))
                print(each_video)
                video_to_jpg(each_video, out_folder, ['r', how_many_random],
                             prefix=prefix)

            elif grab_mode == '-s':
                frames_request = input("Please enter what frames you like?\n"
                                       "Please delimit numbers with \'/\'\n"
                                       "Eg:10/5/357/22\n>>>")
                specific_frames = [int(number) for number in frames_request.split('/')]
                print('Running specific frames with these frames\n{}'.format(specific_frames))
                video_to_jpg(each_video, out_folder, specific_frames, prefix=prefix)
            else:
                print('ERROR: Code, commands not found!')
                return
            print('End of video to image')
            return
    else:
        print('ERROR: Could not find any videos in \'{}\' with extension \'{}\''.format(folder, vid_formats))
        return


if __name__ == "__main__":

    print('Hello\n This is Video2img...')
    main_function()
    print('End Program')
    exit(0)
