"""
Just Drive

This module is used for autonomous driving of the RC car. It is design to support multiple RC cars but this is not tested.
It will also display information in a similar way to how the algorithm sees it.
By default, it will operate for just 60 seconds
"""

import time
import os
import glob
import sys
import socket
import struct
import threading as thrd
import serial
import numpy as np
import cv2
# import matplotlib.pyplot as plt


class Driver(object):
    """
        This is class Driver.

        This class 'Driver' is used as the master to operate the lower level controls of autonomous driving.
    """

    DRIVER_COUNT = 0
    MAX_DRIVERS = 3
    INIT_BAR = thrd.Barrier(3, timeout=90)  # Self (get_latest), camera server(dat+pro), ...

    def __init__(self, tl_xml='../classifier/trained_classifiers/tl_v15_20.xml', nn_npz='./ann.xml'):
        """
            The initiator for the Driver class.

            This function is '__init__' and is used to initiate this class.
            It accepts both the object cascade and the ANN model files (for OpenCV).

            Kwargs:
                draw:           If true then detections will be drawn not just processed. default: True

            Returns:
                An object.
        """

        self.DRIVER_NUMBER = (Driver.DRIVER_COUNT + 1)  # Get driver number
        try:
            if self.DRIVER_NUMBER < Driver.MAX_DRIVERS:
                Driver.DRIVER_COUNT += 1
                print('DRIVER: Driver No:{}'.format(self.DRIVER_NUMBER))
            else:
                raise ValueError('Too many drivers!')
        except ValueError as err:
            print('DRIVER: {}\nMax {} drivers reached!'.format(err, Driver.MAX_DRIVERS))
            exit(0)

        self.port_num = (54320 + self.DRIVER_NUMBER)    # Auto port assign (first one is always 54321)
        if 65535 > self.port_num > 49152:   # Use ephemeral port
            pass
        else:
            self.port_num = 5000    # Reset port number to default if out of range

        try:
            if os.path.isfile(tl_xml):      # Find cascade file if exists
                self.traffic_light_xml = tl_xml
            else:
                raise ValueError('No XML file!')
        except ValueError as err:
            print('DRIVER: {}\n at {}'.format(err, tl_xml))
            exit(0)

        #self.latest_driver_frame = np.fromstring('')
        self.latest_driver_frame = np.zeros((240, 320))
        self.end = False  # When True, threads need to exit cleanly (only set when connection is terminated from client

        self.driver_frame_lock = thrd.RLock()
        self.cam_frame_lock = thrd.RLock()
        self.drive_end_lock = thrd.RLock()

        self.tl_parent_lock = thrd.RLock()
        self.tl_frame_ready = thrd.Event()
        self.traffic_light = TrafficLight(self.traffic_light_xml, self.tl_parent_lock, self.tl_frame_ready, draw=True)
        self.signal = 'green'

        self.driving = thrd.Event()     # Wait till i say go (NN/arduino only, frames can update now)

        self.cam_frame_ready = thrd.Event()     # Used in the 'updater'

        self.cam = ServCam(self.cam_frame_lock, self.cam_frame_ready, port=self.port_num)  # boot up remote camera

        self.remote_controler = Arduino()   # Make and connect to Arduino
        self.remote_controler.find_arduino(write_mode=True)     # Write is true to take control of rc car

        self.nn_parent_lock = thrd.RLock()
        self.nn_frame_ready = thrd.Event()
        self.brain = NeuralNetwork(nn_npz, self.nn_parent_lock, self.nn_frame_ready, draw=True)

        self.auto_mode = False  # if true the driver is in a thread
        self.thrd_get_latest = thrd.Thread(target=self.get_latest)  # Create the threads
        self.thrd_get_latest.start()

        self.thrd_controller = thrd.Thread(target=self.controller)  ## NN/RC controller process
        self.thrd_controller.start()

        self.lifetime = 60.0
        self.thrd_auto_driver = thrd.Thread(target=self.drive, kwargs={'runtime': self.lifetime})

        self.moving = False

        return

    def controller(self):
        """
            The controller is used to communicate with the car and also modify the driving decision for optimum performance.

            Currently it implements a brake function otherwise parses the driving dat straight through without modification.

            Returns:
                None, it will operate the RC car.
        """

        print('DRIVER: THREAD (controller): Initiating driving controls')

        #NN_res = np.zeros((4,), dtype=np.uint8)
        while not self.end:
            # process NN
            with self.nn_parent_lock:
                NN_res = self.brain.nn_choice
            NN_res_bool = NN_res.astype(bool)

            with self.tl_parent_lock:
                local_mode = self.traffic_light.mode
            if local_mode == 'red' or local_mode == 'amb':  # stop car
                if self.moving:
                    self.moving = False
                    self.remote_controler.update_data(FWD=False, BWD=True, LFT=NN_res_bool[2], RIT=NN_res_bool[3])  # Brake
                    time.sleep(0.1)
                self.remote_controler.update_data(FWD=False, BWD=False, LFT=NN_res_bool[2], RIT=NN_res_bool[3])     # Idle
            else:   # ok to go
                self.moving = True
                self.remote_controler.update_data(FWD=NN_res_bool[0], BWD=NN_res_bool[1], LFT=NN_res_bool[2], RIT=NN_res_bool[3])   # Write to the RC car
                time.sleep(0.5)    # Stop hogging the lock, besides the RC car does not react that fast anyway and we want to slow reactions

        self.remote_controler.update_data(FWD=False, BWD=False, LFT=False, RIT=False)
        print('DRIVER: THREAD (controller): Terminating Arduino connection')
        self.remote_controler.end_arduino()
        return

    def get_latest(self):   # Just grab latest frame from cam
        """
            This collects the latest frames for the driver.

            The latest frames are collected and then distributed to relevant classes/objects, using threading locks where needed.
        """

        print('DRIVER: THREAD (get_latest): Driver frame updater is ready...')
        Driver.INIT_BAR.wait()

        while self.cam.count_proc <= 0:     # Wait till first frame is ready
            continue
        self.traffic_light.start_tl()
        self.brain.start_nn()
        print('DRIVER: THREAD (get_latest): Grabbing latest frames')

        while not self.end:     # Update until we have finished

            self.cam_frame_ready.wait(timeout=1.0)     # Wait till the cam notify's there is a new frame available
            self.cam_frame_ready.clear()
            with self.cam_frame_lock:
                temp = self.cam.latest  # Update latest frame buffer from the cam (QVGA - 320W x 240H)
                self.end = self.cam.end     # If cam has stopped then we want to know

            with self.driver_frame_lock:    # Update the latest driver frame
                self.latest_driver_frame = temp

            with self.tl_parent_lock:
                self.traffic_light.current_frame = self.latest_driver_frame[0:120, :]       # Just crop and parse the top half to TL
                self.signal = self.traffic_light.mode  # Update frames on traffic light
            self.tl_frame_ready.set()

            with self.nn_parent_lock:
                #self.brain.current_frame = self.latest_driver_frame[120:240, :]       # Just crop and parse the bottom half to TL
                self.brain.current_frame = cv2.resize(self.latest_driver_frame[120:240, :], (0, 0), fx=0.25, fy=0.25, interpolation=cv2.INTER_AREA)  # Reduce size for ANN (/4)
            self.nn_frame_ready.set()

            #time.sleep(0.02)     # stop hogging the locks/wait till next frame is available - 30ffps is 0.0333 sec

        with self.tl_parent_lock:
            self.traffic_light.current_frame.fill(200)  # Send end flag to the TL thread when finished
            self.traffic_light.end = True
        self.tl_frame_ready.set()   # Avoid hanging

        with self.nn_parent_lock:   # Kill the NN
            self.brain.end = True

        print('DRIVER: THREAD (get_latest): Ended')
        return

    def auto_drive(self):
        """
            This collects the latest frames for the driver.

            The latest frames are collected and then distributed to relevant classes/objects, using threading locks where needed.

            Args:
                self:       This function is part of the class 'Driver'.
                tl_xml:     The LBP/Haar cascade from OpenCV.
                nn_npz:     The OpenCV ANN model file.

            Kwargs:
                draw:           If true then detections will be drawn not just processed. default: True

            Returns:
                An object.

            Examples:
                det_tl = TrafficLight('../trained_classifiers/cascade.xml')
        """

        self.auto_mode = True
        self.thrd_auto_driver.start()   # Start the drive function thread and return
        return

    def drive(self, runtime=120.0):
        """
            This collects the latest frames for the driver.

            The latest frames are collected and then distributed to relevant classes/objects, using threading locks where needed.

            Args:
                self:       This function is part of the class 'Driver'.
                tl_xml:     The LBP/Haar cascade from OpenCV.
                nn_npz:     The OpenCV ANN model file.

            Kwargs:
                draw:           If true then detections will be drawn not just processed. default: True

            Returns:
                An object.

            Examples:
                det_tl = TrafficLight('../trained_classifiers/cascade.xml')
        """

        # Other processing here (stop on red, go on green etc)
        start_time = time.time()
        while ((time.time() - start_time) <= runtime) or not self.end:    # Sit here until  the timer has finished
            continue
        with self.drive_end_lock:
            self.end = True
        self.cam.until_end()
        self.traffic_light.until_end()

        return

    def driver(self, life=60.0, drive_no=1, show=False):
        """
            Sets-up the correct parameters for the driver.

            An ID is used to identify itself and a timeout is used to stop the driver when required.

            Kwargs:
                life:       The lifetime of the driver. default: 60.0
                drive_no:   The driver number. default: 1
                show:       If true it will display the images seen by the driver. default: False
        """

        self.lifetime = life
        print('DRIVER: Timer set: {}Sec'.format(life))
        # self.driving.set()  # Go! (signal all threads)
        print('DRIVER: Go!')
        while self.cam.count_proc <= 0:  # Wait till frames have started (no lock, just read)
            continue
        print('DRIVER: Driver {} has started!'.format(self.DRIVER_NUMBER))

        self.thrd_auto_driver.start()   # Start driving
        if show and (self.DRIVER_NUMBER == drive_no):      # Display driver footage if this is driver 1
            self.show_img()  # Runs until 'end' (blocking)
        self.stop_drive()

        return

    def show_img(self):    # Main thread only
        """
            This shows the received image to the monitor.

            This is a thread and hence operates in a while True loop until the connection is killed from the client
            or the end flag is set.
            Uses the OpenCV imgShow() command to put the image on the display.
            As this is the slowest task it will only show the latest image on every loop so some frames might be dropped/not shown.
            NOTE: This must be ran as part of the main thread as it involves graphics

            Args:
                self:           This function is part of the class 'ServCam'.

            Returns:
                None. Will show image on monitor during stream.

            Examples:
                while not myDriver.end:     # Run this program as the main thread
                    self.show_img()  # Runs until 'end' (blocking)
                self.stop_drive()
        """

        cv2.startWindowThread()  # https://stackoverflow.com/questions/21810452/cv2-imshow-command-doesnt-work-properly-in-opencv-python
        img_to_show = np.zeros((720, 960))
        print('DRIVER: Waiting to show images...')
        while self.cam.count_proc <= 1:
            continue
        print('DRIVER: Showing images for No:{}'.format(self.DRIVER_NUMBER))
        while not self.end:     # This is running in the main thread
            with self.tl_parent_lock:
                img_to_show_th = self.traffic_light.drawn_frame  # Update frames on traffic light
            img_to_show_th = cv2.resize(img_to_show_th, (0, 0), fx=3.0, fy=3.0, interpolation=cv2.INTER_CUBIC)    # Enlarge for show
            with self.nn_parent_lock:
                img_to_show_bh = self.brain.drawn_frame  # Update frames on NN (lane detection)
                img_to_show_bh = cv2.resize(img_to_show_bh, (0, 0), fx=12.0, fy=12.0, interpolation=cv2.INTER_CUBIC)    # 4* plus enlarge for show

            if img_to_show_th.shape == img_to_show_bh.shape:
                img_to_show = np.vstack((img_to_show_th, img_to_show_bh))   # Stick the top half to the bottom half

            cv2.namedWindow('Stream', cv2.WINDOW_NORMAL)    # Must make windows here or will freeze with cascades
            cv2.imshow('Stream', img_to_show)
            cv2.waitKey(5)  # Required to actually process and show image
            cv2.destroyAllWindows()  # Clean up windows
        print('DRIVER: Ending Show')

        return

    def stop_drive(self):
        """
            This function stops the autonomous driving.

            It stops by setting the end flag (signal to all threads) and then waits until they are complete.
        """
        print('DRIVER: Stopping Drive')
        with self.drive_end_lock:
            self.end = True
        #if self.auto_mode:
        #    print('true')
        #    self.thrd_auto_driver.join()
        self.thrd_controller.join()
        self.thrd_get_latest.join()
        self.brain.until_end()
        self.thrd_auto_driver.join()
        print('DRIVER: Drive Stopped!')
        return


class ServCam(object):
    """
        This is class ServCam.

        This class is 'ServCam' is used to accept remote (JPEG) images and process them.
        This class is based on threads to achieve max frame-rate.
        The initial code/idea was taken from the [PiCam docs]
        (https://picamera.readthedocs.io/en/release-1.13/recipes1.html#capturing-to-a-network-stream).
    """

    def __init__(self, parent_lock, parent_event, port=5000, time_out=60.0):
        """
            The initiator for the ServCam class.

            This function is '__init__' and is used to initiate this class.
            This is a blocking function (max 30 secs) and will wait till it receives a TCP connection.
            Initial threading flags are also set here as well as instance variables.

            Args:
                self:           This function is part of the class 'ServCam'.

            Returns:
                An object.

            Examples:
                cam1 = ServCam()
        """

        self.server_socket = socket.socket()
        self.server_socket.bind(('', port))  # Start a socket listening for any connections
        self.server_socket.settimeout(time_out)  # Timeout to 30sec
        self.server_socket.listen(1)  # Reeive a max of 1 connections
        print('SERVCAM: Starting up: on port: {}'.format(port))
        self.connection = self.server_socket.accept()[0].makefile('rb')  # Accept a single connection and make a file-like object out of it
        print('SERVCAM: Connected!')

        self.rec_string = ''    # Data coming in
        self.img_np = np.fromstring(self.rec_string)    # Will be used to convert the data to NP array (as OpenCV image)
        self.latest = np.fromstring('')
        self.count_data = 0
        self.count_proc = 0      # proc count is different as a diff thread is being used - makes it easier than locking/pausing just to count

        # Set up the initial thread order
        self.ready_nxt_frm = thrd.Event()
        self.ready_nxt_frm.set()
        self.ready_pro_frm = thrd.Event()
        self.ready_pro_frm.clear()

        #self.cam_Q = queue.Queue(maxsize=4)     # No more than 4 frames off in buffer

        self.target = 0     # Target FPS

        self.cam_lock = thrd.RLock()
        self.higher_lock = parent_lock
        self.notify_parent = parent_event

        self.thrd_data = thrd.Thread(target=self.get_img)  # Create the threads
        self.thrd_proc = thrd.Thread(target=self.pro_img)
        print('SERVCAM: ServCam is Ready...')
        self.thrd_data.start()
        self.thrd_proc.start()

        self.end = False

    def until_end(self):
        """
            This will wait until the server threads have completed.
        """

        self.thrd_data.join()  # Wait till tasks finished ('end' flag is True)
        self.thrd_proc.join()
        print('SERVCAM: Finished \'ServCam\'')
        return

    def get_img(self):
        """
            This will get an image from the TCP connection.

            This is a thread and hence operates ina while True loop until the connection is killed from the client
            or the end flag is set (currently not set until client closes connection). When ending it will close the connection.
            reads data (an image) based from the length byte and stores it as a string to a variable.

            Args:
                self:           This function is part of the class 'ServCam'.

            Returns:
                None. Will print when reciving data.

            Examples:
                thrd_data = thrd.Thread(target=cam1.get_img)    # Create the threads
                thrd_data.start()
                time.sleep(5)
                thrd_data.join()        # end the tasks
        """

        print('SERVCAM: THREAD (get_img): Ready...')
        Driver.INIT_BAR.wait()    # Wait until other threads are ready
        print('SERVCAM: THREAD (get_img): Running...')

        try:
            self.count_data = 0
            while not self.end:

                image_len = struct.unpack('<L', self.connection.read(struct.calcsize('<L')))[0]  # Read the 'length' byte as a 32-bit unsigned int
                if not image_len:  # If length is zero then quit
                    print('\nSERVCAM: THREAD (get_img): Camera is quiting stream')
                    break
                if image_len < 10:      # Assume that this packet is not a image
                    self.target = int.from_bytes(self.connection.read(image_len), byteorder='little')
                    print('SERVCAM: THREAD (get_img): Target ffps: {}'.format(self.target))
                    continue

                self.count_data += 1
                #print('Received data {}\t'.format(self.count_data), end='')
                self.ready_nxt_frm.wait()  # wait till flag is set (data has been processed)
                self.rec_string = self.connection.read(image_len)   # read the data as a string
                #self.cam_Q.put(self.rec_string)     # Add the frame to the buffer (block if full)
                self.ready_pro_frm.set()    # Data is ready to be processed
                self.ready_nxt_frm.clear()

        finally:
            self.connection.close()
            self.server_socket.close()
            print('SERVCAM: THREAD (get_img): Connection Closed')
            self.ready_pro_frm.set()    # Make sure the processing doesn't hang
            with self.cam_lock:
                self.end = True     # End the threads

        return

    def pro_img(self):
        """
            This will process an image from the instance variable (TCP connection).

            This is a thread and hence operates in a while True loop until the connection is killed from the client
            or the end flag is set.
            Reads the string as a numpy array (UINT8) via OpenCV to decode it into a workable variable.
            It also writes the frame to file

            Args:
                self:           This function is part of the class 'ServCam'.

            Returns:
                Saved image file. Will print when processing data.

            Examples:
                thrd_proc = thrd.Thread(target=cam1.pro_img)    # Create the threads
                thrd_proc.start()
                time.sleep(5)
                thrd_proc.join()        # end the tasks
        """

        self.count_proc = 0
        print('SERVCAM: THREAD (pro_img): Ready...')
        Driver.INIT_BAR.wait()  # Wait until other threads are ready
        print('SERVCAM: THREAD (pro_img): Running...')

        while not self.end:     # As this is a thread just loop here until we should end
            self.ready_pro_frm.wait()  # Wait till frame is ready to process
            #with lock:
            self.img_np = cv2.imdecode(np.fromstring(self.rec_string, np.uint8), cv2.IMREAD_GRAYSCALE)

            self.img_np = cv2.GaussianBlur(self.img_np, (3, 3), 0)      # Blur to remove noise

            #self.img_np = self.cam_Q.get()  # retrive frame
            #self.img_np = cv2.imdecode(np.fromstring(self.rec_string, np.uint8), cv2.IMREAD_GRAYSCALE)
            #self.img_np = cv2.GaussianBlur(self.img_np, (3, 3), 0)      # blur to remove noise

            self.img_np = cv2.resize(self.img_np, (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_AREA)  # Resize to 1/2 of the cam image (VGA to QVGA)
            with self.higher_lock:      # Update latest frame for parent process
                self.latest = self.img_np

            self.count_proc += 1  # Independent count (should never be more than one off the data count)
            self.ready_pro_frm.clear()
            self.ready_nxt_frm.set()  # Image is read for processing so read another frame
            self.notify_parent.set()    # Notify the parent that a new frame is ready (parent should clear/wait on this event)

            #print('Processed Image {}'.format(self.count_proc))
        print('SERVCAM: THREAD (pro_img): Ended')

        return


class TrafficLight(object):
    """
        This is class TrafficLight.

        This class 'TrafficLight' is used to detect traffic lights in a given frame
        and process them to calculate the mode of operation.
    """

    def __init__(self, tl_cascade, parent_lock, parent_event, draw=True):
        """
            The initiator for the TrafficLight class.

            This function is '__init__' and is used to initiate this class.
            It accepts the OpenCV cascade file which will be used for detections (as well as some post processing).

            Args:
                self:           This function is part of the class 'ServCam'.
                tl_cascade:     The LBP/Haar cascade from OpenCV.
                parent_lock:    A threading lock used to share driving data.
                parent_event:   A threading event for informing about new information.

            Kwargs:
                draw:           If true then detections will be drawn not just processed. default: True

            Returns:
                An object.

            Examples:
                det_tl = TrafficLight('../trained_classifiers/cascade.xml')
        """

        self.end = False

        self.detector = cv2.CascadeClassifier(tl_cascade)
        self.min_si = (6, 14)
        self.max_si = (50, 115)
        self.SF = 1.01
        self.conf = 5

        self.tl_lock = thrd.RLock()
        self.detections = np.zeros((1, 5), dtype=int)
        self.current_frame = np.zeros((240, 320), dtype=np.uint8)
        # Rely on the upper lock/process to update this frame

        self.reds = np.ones((5, ))
        self.ambs = np.ones((2, ))     # Amber is on for less time so average for less
        self.grns = np.ones((5, ))

        self.upper_lock = parent_lock
        self.tl_new_frame = parent_event    # Wait till notified from parent that there is a new frame ready (if we ever run that fast)
        self.tl_draw_ready = thrd.Event()   # Only draw if the new frame is ready (if we ever run fast enough)
        self.tl_draw_ready.set()
        self.mode = 'green'
        self.i_draw = draw
        self.drawn_frame = self.current_frame[0:120, :]       # Latest frame with drawing(s)
        self.END_SUM = (self.drawn_frame.shape[0] * self.drawn_frame.shape[1] * 200)
        print("TRFLI: End Sum: {} ({} all \'200\' uint8)".format(self.END_SUM, self.drawn_frame.shape))
        if self.i_draw:
            print('TRFLI: Created Drawing Thread!')
        self.thrd_tl_draw = thrd.Thread(target=self.draw_tl_det)

        self.thrd_tl_proc = thrd.Thread(target=self.tl_mode)
        print('TRFLI: Created a Traffic Light Processor Thread!')

        return

    def start_tl(self):
        """
            This will start the traffic light threads.
        """
        self.thrd_tl_proc.start()
        if self.i_draw:
            self.thrd_tl_draw.start()
        return

    def until_end(self):
        """
            This will wait until the traffic light threads have completed.
        """

        self.thrd_tl_proc.join()  # Wait till tasks finished ('end' flag is True)
        self.thrd_tl_draw.join()
        print('TRFLI: Finished \'Traffic Light\'')
        return

    def get_det(self, det_img, SF=1.01, conf=3):
        """
            This will get the traffic light detections.

            Given an input image, the cascade file will be used to detect any traffic lights.
            With any lights found, it will then also calculate the mode of operation (red, green etc.)

            Args:
                det_img:    The image which needs to be scanned for detections.

            Kwargs:
                SF:         Scale Factor parameter for objected detection. default: 1.01
                conf:       Confidence parameter for objected detection. default: 3

            Returns:
                Nothing, the detections and traffic light modes are stored inside the class

            Examples:
                self.get_det(shadow_frame, SF=self.SF, conf=self.conf)
        """

        del_list = []
        det_cnt = 0
        #tl_crop = det_img[0:120, :]     # Just look at areas where TL wil be
        cv_img_detecs = self.detector.detectMultiScale(det_img, SF, 0, minSize=self.min_si, maxSize=self.max_si)

        obj_det, obj_scr = cv2.groupRectangles(np.array(cv_img_detecs).tolist(), conf, 1.0)
        cv_obj = np.hstack((obj_det, obj_scr))  # Conjoin the score to the detection [x, y, w, h, scr]

        for (detx, dety, detw, deth, detscr) in reversed(cv_obj):  # Draw all the detections
            crop_img = det_img[dety:dety + deth, detx:detx + detw]  # Where (x1,y1) is top left of crop and (x2,y2) is bottom right of crop)

            if (not np.any(crop_img > 180)) or detscr < conf:
                del_list.append(det_cnt)
                det_cnt += 1
                continue

        for each in reversed(del_list):   # Remove all false predictions (based on post proc if bright light)
            cv_obj = np.delete(cv_obj, each, axis=0)
        #top_res = cv_obj[np.argmax(cv_obj[:, -1:]), ]  # Return the one top result only

        with self.tl_lock:
            self.detections = cv_obj    # For drawing in other thread

        # Split dets down to thirds and work out what light is on (use cv_obj)
        for (detx, dety, detw, deth, detscr) in cv_obj:
            crop_img = det_img[dety:dety + deth, detx:detx + detw]
            third_y = int(deth / 3)

            w1 = int(detw * 0.3)
            w2 = int(detw * 0.7)

            red_img = crop_img[int(deth * 0.1):third_y, w1:w2]    # Remove 10% from 7 an 20% from x to get closer to the light
            amb_img = crop_img[int(1.1 * third_y):int(1.9 * third_y), w1:w2]
            grn_img = crop_img[int(1.9 * third_y):int(deth * 0.8), w1:w2]

            red = np.mean(red_img)  # Average brightness
            self.reds = np.append(self.reds, red)[1:]     # Circular buffer
            red = np.mean(self.reds)    # Average value over time

            amb = np.mean(amb_img)
            self.ambs = np.append(self.ambs, amb)[1:]
            amb = np.mean(self.ambs)

            grn = np.mean(grn_img)
            self.grns = np.append(self.grns, grn)[1:]
            grn = np.mean(self.grns)

            red_2_amb = abs(red - amb)
            amb_2_grn = abs(amb - grn)
            large_stp = ((red + amb + grn) / 3) * 0.25  # Large step is 25% of the average value

            amb_flg = False
            if red_2_amb < large_stp < amb_2_grn:
                amb_flg = True

            my_mode = 'none'
            if np.any(grn_img > 180) and grn == max(grn, amb, red):     # If there is a bight spot and is the brightest
                my_mode = 'green'
            elif np.any(amb_img > 180) and amb == max(grn, amb, red):
                if amb_flg:
                    my_mode = 'red_amb'
                else:
                    my_mode = 'amber'
            elif np.any(red_img > 180) and red == max(grn, amb, red):
                if amb_flg:
                    my_mode = 'red_amb'
                else:
                    my_mode = 'red'

            with self.upper_lock:
                self.mode = my_mode     # For parent information

        return

    def tl_mode(self):
        """
            This will call the traffic light detections.
        """

        print('TRFLI: THREAD (tl_mode): Starting Traffic Light Detector')

        while not self.end:

            self.tl_new_frame.wait(timeout=0.05)    # Wait till parent notifies us

            with self.upper_lock:   # Grab latest frame
                shadow_frame = self.current_frame
            #print(shadow_frame.sum())
            self.tl_new_frame.clear()

            self.get_det(shadow_frame, SF=self.SF, conf=self.conf)
            self.tl_draw_ready.set()  # You can start drawing if your ready

            #time.sleep(0.01)  # Stop hogging the lock 30ffps is 0.033 Sec
        print('TRFLI: THREAD (tl_mode): Ended')
        return

    def draw_tl_det(self):
        """
            This will draw the traffic light detections.

            This will modify a frame with drawn on detections for visualization.
        """

        print('TRFLI: THREAD (draw_tl_det): Starting Traffic Light Drawing')
        while not self.end:

            self.tl_draw_ready.wait(timeout=0.4)
            self.tl_draw_ready.clear()
            with self.upper_lock:
                signal = self.mode
                draw_img = self.current_frame
            #print(draw_img)
            #draw_img = draw_img[0:120, :]

            with self.tl_lock:
                rect_obj = self.detections
            for (detx, dety, detw, deth, detscr) in rect_obj:  # Draw all the detections
                draw_img = cv2.rectangle(draw_img, (detx, dety), (detx + detw, dety + deth), (255, 255, 255), 1)  # Draw a rectangle around detection
                draw_img = cv2.putText(draw_img, '{}'.format(detscr), (detx, dety), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
                draw_img = cv2.putText(draw_img, '{}'.format(signal), (detx + detw, dety + deth), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
            with self.upper_lock:
                self.drawn_frame = draw_img

            #time.sleep(0.02)     # Stop hogging the lock (drawing is alot less important than the raw detections) 30ffps is 0.033 Sec

        print('TRFLI: THREAD (draw_tl_det): Finished Drawing')
        return  # Just write the frame to local and exit thread


class NeuralNetwork(object):
    """
        This is class NeuralNetwork.

        This class 'NeuralNetwork' is used to process the NeuralNetwork data from a image into usable driving information.
    """

    def __init__(self, nn_nets, parent_lock, parent_event, draw=True):
        """
            The initiator for the NeuralNetwork class.

            This function is '__init__' and is used to initiate this class.

            Args:
                self:           This function is part of the class 'NeuralNetwork'.
                nn_nets:        The OpenCV ANN model file.
                parent_lock:    A threading lock used to share driving data.
                parent_event:   A threading event for informing about new information.

            Kwargs:
                draw:           If true then ANN driving will be drawn not just processed. default: True

            Returns:
                An object.

            Examples:
                        self.brain = NeuralNetwork(nn_npz, self.nn_parent_lock, self.nn_frame_ready, draw=True)
        """

        self.end = False
        self.conf = 0.55

        try:
            #if os.path.isfile(nn_nets):      # Find cascade file if exists
            if True:
                self.nn_file = nn_nets
            else:
                raise ValueError('No NPZ file!')
        except ValueError as err:
            print('DRIVER: {}\n at {}'.format(err, nn_nets))
            exit(0)

        # declare the nn
        self.nn_model = cv2.ml.ANN_MLP_create()
        self.nn_model.setLayerSizes(np.array([2400, 64, 4], dtype=np.uint16))
        print(self.nn_model.getLayerSizes())
        self.nn_model.setActivationFunction(cv2.ml.ANN_MLP_SIGMOID_SYM, 2, 1)

        self.nn_model = cv2.ml.ANN_MLP_load(self.nn_file)

        self.nn_choice = np.zeros((4,), dtype=np.uint8)     # Output results

        self.nn_lock = thrd.RLock()
        self.detections = np.zeros((1, 5), dtype=int)

        self.current_frame = np.zeros((30, 80), dtype=np.uint8)     # Blank temp image
        # Rely on the upper lock/process to update this frame

        self.upper_lock = parent_lock
        self.nn_new_frame = parent_event    # Wait till notified from parent that there is a new frame ready (if we ever run that fast)
        self.nn_draw_ready = thrd.Event()   # Only draw if the new frame is ready (if we ever run fast enough)
        self.nn_draw_ready.set()
        self.i_draw = draw
        self.drawn_frame = self.current_frame       # Latest frame with drawing(s)
        if self.i_draw:
            print('NNET: Created Drawing Thread!')
        self.thrd_nn_draw = thrd.Thread(target=self.draw_nn_det)

        self.thrd_predict = thrd.Thread(target=self.predict, args=(self.current_frame,))
        print('NNET: Created a Traffic Light Processor Thread!')

        return

    def start_nn(self):
        """
            This will start the NeuralNetwork threads.
        """

        self.thrd_predict.start()
        if self.i_draw:
            self.thrd_nn_draw.start()
        return

    def until_end(self):
        """
            This will wait until the NeuralNetwork threads have completed.
        """

        self.thrd_predict.join()  # Wait till tasks finished ('end' flag is True)
        self.thrd_nn_draw.join()
        print('NNET: Finished \'Neural Network\'')
        return

    def predict(self):
        """
            This will use the ANN to predict the correct outputs.

            This will use the ANN to predict the correct outputs. The outputs are then modified to suit an expected driving choice.

            Returns:
                None, both the input & output data is used internally by the class
        """

        print('NNET: THREAD (predict): Starting Neural Network Predictions')
        while not self.end:



            self.nn_new_frame.wait(timeout=0.4)  # Wait till new frame or timeout
            self.nn_new_frame.clear()
            with self.upper_lock:
                nn_img = self.current_frame

            nn_img = self.detect_lane(nn_img, origin=False)

            #resp = np.random.uniform(low=0.1, high=1.0, size=(4,))  # simulate NN
            #time.sleep(1.0)  # for simulation just slow down number of changes per second
            layer_in = nn_img.reshape(1, 2400).astype(np.float32)
            #layer_in = np.vstack((layer_in, layer_in))
            ret, resp = self.nn_model.predict(layer_in)
            #print(resp)
            resp[resp <= 0.2] = 0   # Threshold these (make a choice)
            resp[resp > 0.2] = 1
            resp = resp.flatten()
            #print(resp)

            # Forward / Backward
            larger_fwdbwd_idx = np.argmax(resp[0:2])  # See what is larger
            smaller_fwdbwd_idx = np.argmin(resp[0:2])
            if (resp[larger_fwdbwd_idx] - resp[smaller_fwdbwd_idx]) >= self.conf:  # Find the difference (total conf) and see if it meets our confidence level
                with self.upper_lock:
                    self.nn_choice[larger_fwdbwd_idx] = 1.0
                    self.nn_choice[smaller_fwdbwd_idx] = 0.0
            else:  # Neither reached our level so just be safe and right a zero
                with self.upper_lock:
                    self.nn_choice[larger_fwdbwd_idx] = 0.0
                    self.nn_choice[smaller_fwdbwd_idx] = 0.0

            # Left / Right
            larger_lftrit_idx = np.argmax(resp[2:])  # See what is larger
            smaller_lftrit_idx = np.argmin(resp[2:])
            if (resp[2 + larger_lftrit_idx] - resp[2 + smaller_lftrit_idx]) >= self.conf:  # Find the difference (total conf) and see if it meets our confidence level
                with self.upper_lock:
                    self.nn_choice[2 + larger_lftrit_idx] = 1.0
                    self.nn_choice[2 + smaller_lftrit_idx] = 0.0
            else:  # Neither reached our level so just be safe and right a zero
                with self.upper_lock:
                    self.nn_choice[2 + larger_lftrit_idx] = 0.0
                    self.nn_choice[2 + smaller_lftrit_idx] = 0.0

        print('NNET: THREAD (predict): Finished Predictions')
        return

    def draw_nn_det(self):
        """
            This will draw the ANN predictions.

            This will modify a frame with drawn on predictions for visualization.
        """

        arrow_length = 10
        center = (40, 20)  # x,y for start of arrow (bottom centre image) (0,0) is top left
        arr_left = (center[0] - arrow_length, center[1])
        arr_right = (center[0] + arrow_length, center[1])
        arr_up = (center[0], center[1] - arrow_length)
        arr_down = (center[0], center[1] + int(arrow_length / 2))  # Down is half length as arrow is close to bottom

        print('NNET: THREAD (draw_nn_det): Starting Neural Network Drawing')
        while not self.end:
            #https: // docs.opencv.org / 3.0 - beta / modules / imgproc / doc / drawing_functions.html  # Arrowedline
            time.sleep(0.2)     # 5fps is fast enough for this displaying arrows

            self.nn_new_frame.wait(timeout=0.4)     # Wait till new frame or timeout
            self.nn_new_frame.clear()

            with self.upper_lock:
                latest_nn_img = self.current_frame

            latest_nn_img = self.detect_lane(latest_nn_img, origin=True)   # Process lane detection with "latest_nn_img" here

            with self.upper_lock:   # Is this lock required for reading?
                if self.nn_choice[0]:   # If forward
                    latest_nn_img = cv2.arrowedLine(latest_nn_img, center, arr_up, (200, 0, 0), thickness=1, tipLength=0.3)
                if self.nn_choice[1]:   # If backward
                    latest_nn_img = cv2.arrowedLine(latest_nn_img, center, arr_down, (200, 0, 0), thickness=1, tipLength=0.3)
                if self.nn_choice[2]:   # If left
                    latest_nn_img = cv2.arrowedLine(latest_nn_img, center, arr_left, (200, 0, 0), thickness=1, tipLength=0.3)
                if self.nn_choice[3]:   # If right
                    latest_nn_img = cv2.arrowedLine(latest_nn_img, center, arr_right, (200, 0, 0), thickness=1, tipLength=0.3)

            with self.upper_lock:
                self.drawn_frame = latest_nn_img    # Write the latest frame to the master

        print('NNET: THREAD (draw_nn_det): Finished Drawing')
        return  # Just write the frame to local and exit thread

    def detect_lane(self, in_img, origin=False):
        """
            This will process the lane detction on a given image.

            The image is first scaled down by a factor of 4 before using, blurring, thresholding and Hough lines to detect the lanes.
            It will then draw the detected lanes on the image before being returned.

            Args:
                in_img:     Input image to process.

            Kwargs:
                origin:     If false then a blank image is used to draw the lanes, else the original is used. default: False

            Returns:
                the image with lanes drawn on.

            Examples:
                lane_vision = self.detect_lane(roi)
        """

        orig = in_img
        nn_img = np.full((30, 80), 255, dtype=np.uint8)     # Make a white image

        in_img = cv2.medianBlur(in_img, 3)
        in_img = cv2.adaptiveThreshold(in_img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 13, 7)
        in_img = cv2.bitwise_not(in_img)    # If no canny then must do this (lines should be white)

        linesP = cv2.HoughLinesP(in_img, 1, (1 * np.pi) / 180, 20, None, 8, 5)

        if not origin:
            if linesP is not None:
                for i in range(0, len(linesP)):
                    l = linesP[i][0]
                    cv2.line(nn_img, (l[0], l[1]), (l[2], l[3]), (0, 0, 0), 2, cv2.LINE_AA)  # Draw black lines
            return nn_img
        else:
            if linesP is not None:
                for i in range(0, len(linesP)):
                    l = linesP[i][0]
                    cv2.line(orig, (l[0], l[1]), (l[2], l[3]), (0, 0, 0), 2, cv2.LINE_AA)  # Draw lines on original
        return orig


class Arduino(object):
    """
        This is class Arduino.

        This class 'Arduino' is used to connect and communicate with an Arduino via USB.
    """

    ARD_SER_PARM = dict(
        # port=None,    # Not needed - define port at instant
        baudrate=9600,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=0.2
    )

    ARD_CMD_MODE2 = int('10000000', 2)
    ARD_CMD_FWD2 = int('00001000', 2)
    ARD_CMD_BWD2 = int('00000100', 2)
    ARD_CMD_LFT2 = int('00000010', 2)
    ARD_CMD_RIT2 = int('00000001', 2)

    ARD_CMD_MODE = 128
    ARD_CMD_FWD = 8
    ARD_CMD_BWD = 4
    ARD_CMD_LFT = 2
    ARD_CMD_RIT = 1

    ARD_READ = b'\x80'

    ARD_READ_ACK = b'\x7F'

    ARD_WRITE = b'\x00'
    ARD_WRITE_ACK = b'\xFF'

    BOOT_DELAY = 2.0

    def __init__(self):
        """
            The initiator for the Arduino class.

            This function is '__init__' and is used to initiate this class.
            It creates a serial object for communication but does not attempt to communicate.

            Args:
                self:           This function is part of the class 'Arduino'.

            Returns:
                An object.

            Examples:
                controller = Arduino()
        """

        self.myport = None      # Save the port I am on
        self.databuffer = []
        self.myser = serial.Serial(**Arduino.ARD_SER_PARM)       # Serial Object for communicating
        self.read_mode = True
        #self.serial_buffer = bytearray(b'')
        self.serial_byte = b''

        self.FWD = False
        self.BWD = False
        self.LFT = False
        self.RIT = False

        self.rc_state = np.zeros((4,), dtype=np.uint8)
        print('ARDUINO: Created!')

    @staticmethod
    def all_serial_ports():
        """Returns all open serial ports as a list

        Will check the environment your running on, and return a list of all available ports.
        Note that this function is never directly called.

        Returns:
            A list of all available ports e.g. COM1, COM11, COM12....

        Examples:
            ``>>> allports = all_serial_ports()``\n
            ``>>> print(allports, sep= ', ')``
        """

        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            # this excludes your current terminal "/dev/tty"
            ports = glob.glob('/dev/tty[A-Za-z]*')
        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')
        else:
            raise EnvironmentError('Unsupported platform')

        result = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                result.append(port)
            except (OSError, serial.SerialException):
                pass
        return result

    def find_arduino(self, write_mode=False):  # Will find the first Arduino on the open serial ports
        """
            This will test all available serial ports to find the connected Arduino.

            A known ping and answer is used to find the Arduino
            , if multiple Arduinos are connected, only the first available one will be used.

            Args:
                self:           This function is part of the class 'Arduino'.

            Kwargs:
                write_mode:     Sets the Arduinos mode of operation. default: False

            Returns:
                Nothing, but leaves the correct serial port open for communication.

            Examples:
                slave = Arduino()
                slave.find_arduino()
        """

        print("ARDUINO: Finding Arduino...\nCurrently available serial ports:")
        ser_list = self.all_serial_ports()  # Grab the list of available ports
        print(ser_list)

        for each_port in reversed(ser_list):
            self.myser = serial.Serial(**Arduino.ARD_SER_PARM, port=each_port)  # Use standard serial settings
            time.sleep(Arduino.BOOT_DELAY)     # Required as Arduino resets on port open

            print((("ARDUINO: Trying {} ...").format(each_port)))
            if write_mode:
                self.myser.write(Arduino.ARD_WRITE)  # Send out the query
                self.read_mode = False
            else:
                self.myser.write(Arduino.ARD_READ)
                self.read_mode = True

            self.serial_byte = self.myser.read(1)  # Only ever sending 1 byte (ACK)

            if (write_mode and self.serial_byte == Arduino.ARD_WRITE_ACK) or (self.serial_byte == Arduino.ARD_READ_ACK):
                self.myport = each_port  # Remember what port I am on
                print('ARDUINO: We found Arduino on: {}'.format(self.myport))
                print(self.serial_byte)
                #self.myser.close()     # Leave port open to avoid resetting
                return True       # found first result
            else:
                self.myser.close()

        self.myser.close()
        return False

    def update_data(self, FWD=False, BWD=False, LFT=False, RIT=False):
        """
            This will retrive or send the latest information to the Arduino.

            Reading or writing is determined by 'self.read_mode'.

            Args:
                self:           This function is part of the class 'Arduino'.

            Returns:
                Nothing, but updates its internal state.

            Examples:
                while(current_time - previous_time) <= 20.0:    # do for 10 sec
                    current_time = time.time()
                    slave.update_data()
        """

        if self.read_mode:
            self.serial_byte = self.myser.read(1)   # Receive the one byte reply
            my_int = int.from_bytes(self.serial_byte, byteorder='big')  # Form it into a usable type (int)

            self.rc_state[3] = ((my_int & Arduino.ARD_CMD_FWD) >> 3)    # Bit mask it to get the true data)
            self.rc_state[2] = ((my_int & Arduino.ARD_CMD_BWD) >> 2)
            self.rc_state[1] = ((my_int & Arduino.ARD_CMD_LFT) >> 1)
            self.rc_state[0] = (my_int & Arduino.ARD_CMD_RIT)
            #print(self.rc_state)
            self.myser.write(Arduino.ARD_READ)  # Send command ready for next read (assume we will back here very soon)
        else:   # Assume writing mode

            command = 0
            if FWD and BWD:     # Remove illegal states
                FWD = False
                BWD = False
            if LFT and RIT:
                LFT = False
                RIT = False

            if FWD:
                self.FWD = True     # Update internal states
                command = (Arduino.ARD_CMD_FWD | command)   # Start to construct the byte
            if BWD:
                self.BWD = False
                command = (Arduino.ARD_CMD_BWD | command)
            if LFT:
                self.LFT = False
                command = (Arduino.ARD_CMD_LFT | command)
            if RIT:
                self.RIT = False
                command = (Arduino.ARD_CMD_RIT | command)

            packet = struct.pack("B", command)  # Pack it into 1 byte (8
            self.myser.write(packet)    # Send to Arduino

        return

    def end_arduino(self):

        self.myser.close()     # Leave port open to avoid resetting
        return


def main_function():
    """
        This is the main function.

        This function is the main function (if using this file standalone).
        It creates one autonomous driver for 60 secons whilst displaying the data seen.
    """

    print('MAINFUNC: Creating Driver \'Callahan\'')
    callahan = Driver()
    print('MIANFUNC: Starting Callahan')
    callahan.driver(life=60.0, show=True)    # Show is blocking
    print('MAINFUNC: \'Callahan\' has finished')

    return


if __name__ == "__main__":
    print('MAIN: Hello')
    print('MAIN: This is \'driver_2.py\'\nSTART:')
    main_function()
    print('MAIN: End Program')
    exit(0)
