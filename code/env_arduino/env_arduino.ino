
//#define DEBUG 1   // uncomment for debuging via serial

// Set pin numbers:
#define ledTest LED_BUILTIN  /*!< Built-in LED Pin (usually pin 13)*/

#define ledRed1 11  /*!< TL1: Red */
#define ledAmr1 10  /*!< TL1: Amber */
#define ledGrn1 9  /*!< TL1: Green */

#define ledRed2 6  /*!< TL2: Red */
#define ledAmr2 5  /*!< TL2: Amber */
#define ledGrn2 3  /*!< TL2: Green */

#define rcFwdPin 8  /*!< RC remote foward pin */
#define rcBwdPin 7  /*!< RC remote backward pin */
#define rcLftPin 4  /*!< RC remote left pin */
#define rcRitPin 2  /*!< RC remote right pin */

#define currentMillis millis()   /*!< placeholder for the current time (milliseconds) */
#define interval_default 10000    /*!< defualt lights tick time (milliseconds) */
unsigned long previousMillis = 0;        /*!< will store last time LED was updated */
long interval = interval_default;           /*!< interval at which to blink/change (milliseconds) */

#define redval 8    // PWM value for brightness
#define ambval 22
#define grnval 5

bool amber_mode = false;   /*!< If amber is on then a short tick is needed */

#define mskMode B10000000   // 7
#define mskFwd  B00001000   // 3
#define mskBwd  B00000100   // 2
#define mskLft  B00000010   // 1
#define mskRit  B00000001   // 0

#define modeRead 1
#define modeWrite 0
#define Fwd 1
#define Bwd 0
#define Rit 1
#define Lft 0
uint8_t command = 0;
#define cmdMode ((mskMode & command) >> 7)  // returns 1 or 0
#define rcFwd   ((mskFwdBwd & command) >> 3)  // 1=fwd
#define rcBwd   ((mskFwdBwd & command) >> 2)   
#define rclft   ((mskLftRit & command) >> 1)   
#define rcRit   ((mskFwdBwd & command) >> 0)  
#define rcAll   (B00001111 & command)   // return all at once (same format as array
#define rcLR   (B00001100 & command)
#define rcFB   (B00000011 & command)

#define serAck (command ^ 255)

#define rcWriteStp 0
#define rcWriteFwd 8
#define rcWriteBwd 4
#define rcWriteLft 2
#define rcWriteRit 1

#define none 5

bool rcState[4] = {0, 0, 0, 0}; //{F, B, L, R}
#define FWD 0
#define BWD 1
#define LFT 2
#define RIT 3

uint8_t fixCmdMode = 100;

void setup() {
    /** \brief Standard setup function.
      * \param None
      * \return None
      * 
      * This function sets up all the LED's as outputs.  
      * ...
      */
 
  Serial.begin(9600);
    while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  #ifdef DEBUG
    Serial.print("setup\n");
  #endif
  
  controlMode(false);   // Set RC pin to input (high impediance to start)

  // set the digital pin as output:
  pinMode(ledTest, OUTPUT);
  pinMode(ledRed1, OUTPUT);
  pinMode(ledAmr1, OUTPUT);
  pinMode(ledGrn1, OUTPUT);
  pinMode(ledRed2, OUTPUT);
  pinMode(ledAmr2, OUTPUT);
  pinMode(ledGrn2, OUTPUT);
  // ensure outputs begin low
  digitalWrite(ledTest, LOW); 
  digitalWrite(ledRed1, LOW);
  digitalWrite(ledAmr1, LOW);
  digitalWrite(ledGrn1, LOW);
  digitalWrite(ledRed2, LOW);
  digitalWrite(ledAmr2, LOW);
  digitalWrite(ledGrn2, LOW);
  
  // Check for serial comms here first
  previousMillis = currentMillis;
  while ((currentMillis - previousMillis) <= 5000){
    if (Serial.available()) {
      command = Serial.read();
      if (cmdMode == modeWrite){
        fixCmdMode = modeWrite;
        controlMode(true);
        Serial.write(serAck);    // ACK is XOR of command
        break;
      }
      else{  
        fixCmdMode = modeRead;
        controlMode(false);
        Serial.write(serAck);    // ACK is XOR of command
        break;
      }
    
    }
    else {    
      fixCmdMode = 255;
      controlMode(false);
    }
  }
  
  #ifdef DEBUG
    Serial.print("\nTesting RC\n");
    testLights();  // run the intial test sequence
    controlMode(true);
    testRc();
    controlMode(false);
    Serial.print("\nTesting RC Done!\n");
  #endif
  
  
  #ifdef DEBUG
    Serial.print("run main\n");
  #endif
} 


void rcWrite(uint8_t lR, uint8_t fB){

  switch (fB){
    case rcWriteFwd:
      #ifdef DEBUG
        Serial.print("^^");
      #endif
      pinMode(rcFwdPin, OUTPUT);
      digitalWrite(rcFwdPin, LOW);    // Pull low to turn on
      pinMode(rcBwdPin, INPUT);
      digitalWrite(rcBwdPin, HIGH);
      break;
   case rcWriteBwd:
      #ifdef DEBUG
        Serial.print("%%");
      #endif
      pinMode(rcFwdPin, INPUT);
      digitalWrite(rcFwdPin, HIGH);
      pinMode(rcBwdPin, OUTPUT);
      digitalWrite(rcBwdPin, LOW);
      break;
    case none:
      #ifdef DEBUG
        Serial.print("  ");
      #endif
      pinMode(rcFwdPin, INPUT);
      digitalWrite(rcFwdPin, HIGH);
      pinMode(rcBwdPin, INPUT);
      digitalWrite(rcBwdPin, HIGH);
      break;
    case rcWriteStp:
      #ifdef DEBUG
        Serial.print("##");
      #endif
      pinMode(rcFwdPin, INPUT);
      digitalWrite(rcFwdPin, HIGH);
      pinMode(rcBwdPin, INPUT);
      digitalWrite(rcBwdPin, HIGH);
      break;
    default:
      pinMode(rcFwdPin, INPUT);
      digitalWrite(rcFwdPin, HIGH);
      pinMode(rcBwdPin, INPUT);
      digitalWrite(rcBwdPin, HIGH);
      break;
  }
  
switch (lR){
   case rcWriteLft:
      #ifdef DEBUG
        Serial.print("<-");
      #endif
      pinMode(rcLftPin, OUTPUT);
      digitalWrite(rcLftPin, LOW);
      pinMode(rcRitPin, INPUT);
      digitalWrite(rcRitPin, HIGH);
      break; 
    case rcWriteRit:
      #ifdef DEBUG
        Serial.print("->");
      #endif
      pinMode(rcLftPin, INPUT);
      digitalWrite(rcLftPin, HIGH);
      pinMode(rcRitPin, OUTPUT);
      digitalWrite(rcRitPin, LOW);
      break;
    case none:
      #ifdef DEBUG
        Serial.print("  ");
      #endif
      pinMode(rcLftPin, INPUT);
      digitalWrite(rcLftPin, HIGH);
      pinMode(rcRitPin, INPUT);
      digitalWrite(rcRitPin, HIGH);
      break;
    case rcWriteStp:
      #ifdef DEBUG
        Serial.print("##");
      #endif
      pinMode(rcLftPin, INPUT);
      digitalWrite(rcLftPin, HIGH);
      pinMode(rcRitPin, INPUT);
      digitalWrite(rcRitPin, HIGH);
      break;
    default:
      pinMode(rcLftPin, INPUT);
      digitalWrite(rcLftPin, HIGH);
      pinMode(rcRitPin, INPUT);
      digitalWrite(rcRitPin, HIGH);
      break;
  }

}

void testRc(void){
  
unsigned long testTimeCur = millis();
unsigned long testTimePre = testTimeCur;
uint8_t rcTestNum = 0;
bool contFlg = true;
const long testTime = 800;

#ifdef DEBUG
  Serial.print(testTimeCur);
  Serial.print("\t");
  Serial.print(rcTestNum);
  Serial.print("\n");
#endif

  while (contFlg){
    switch (rcTestNum){
      case 0:
        rcWrite(rcWriteLft, none);
        while ((testTimeCur - testTimePre) <= testTime){testTimeCur = millis();}   
        break;
      case 1:
        rcWrite(rcWriteRit, none);
        while ((testTimeCur - testTimePre) <= testTime){testTimeCur = millis();}
        break;
      case 2:
        rcWrite(none, rcWriteFwd);
        while ((testTimeCur - testTimePre) <= testTime){testTimeCur = millis();}
        break;
      case 3:
        rcWrite(none, rcWriteBwd);
        while ((testTimeCur - testTimePre) <= testTime){testTimeCur = millis();}
        break;
      case 4:
        rcWrite(rcWriteLft, rcWriteFwd);
        while ((testTimeCur - testTimePre) <= testTime){testTimeCur = millis();}
        break;
      case 5:
        rcWrite(rcWriteLft, rcWriteBwd);
        while ((testTimeCur - testTimePre) <= testTime){testTimeCur = millis();}
        break;
      case 6:
        rcWrite(rcWriteRit, rcWriteFwd);
        while ((testTimeCur - testTimePre) <= testTime){testTimeCur = millis();}
        break;
      case 7:
        rcWrite(rcWriteRit, rcWriteBwd);
        while ((testTimeCur - testTimePre) <= testTime){testTimeCur = millis();}
        break;
      default:
        rcWrite(rcWriteStp, rcWriteStp);
    }
      testTimeCur = millis();
      rcTestNum += 1; 
      testTimePre = testTimeCur;
      #ifdef DEBUG
        Serial.print(testTimeCur);
        Serial.print("\t");
        Serial.print(rcTestNum);
        Serial.print("\n");
      #endif    
      if (rcTestNum > 7) {rcWrite(rcWriteStp, rcWriteStp);  contFlg = false;} // break from while loop 
  }
  
}

void controlMode(bool rcout){

  if (rcout){
    #ifdef DEBUG
      Serial.print("rc -- OUTPUT\n");
    #endif
    pinMode(rcFwdPin, OUTPUT);    // Set RC pin to output to take control
    pinMode(rcBwdPin, OUTPUT);
    pinMode(rcLftPin, OUTPUT);
    pinMode(rcRitPin, OUTPUT);

    digitalWrite(rcFwdPin, HIGH);    // Pull low to turn on
    digitalWrite(rcBwdPin, HIGH);
    digitalWrite(rcLftPin, HIGH);
    digitalWrite(rcRitPin, HIGH);
  }
  else{
    #ifdef DEBUG
      Serial.print("rc -- INPUT\n");
    #endif
    pinMode(rcFwdPin, INPUT);    // Set RC pin to input (high impediance to start)
    digitalWrite(rcFwdPin, HIGH);   // internal pull up
    pinMode(rcBwdPin, INPUT);
    digitalWrite(rcBwdPin, HIGH);
    pinMode(rcLftPin, INPUT);
    digitalWrite(rcLftPin, HIGH);
    pinMode(rcRitPin, INPUT);
   digitalWrite(rcRitPin, HIGH); 
  }

}

void testLights() {
    /** \brief Test the traffic lights operation.
      * \param None
      * \return None
      * 
      * This function tests the traffic lights by first turning on for 5 seconds then individually flashing for 3 then all flash for 2.  
      * ...
      */

      const long test_tick = 500;   /*!< local, tick counter in millis */
      int tick_count = 0;   /*!< local counter */
      bool test_mode = true;  /*!< test untill false */
      #define test_time (10000 / test_tick)  //10 sec (20tk)
      #define mode1 (5000 / test_tick)   //5 sec (10tk)
      #define mode2 (mode1 + (3000 / test_tick))   //3 sec (6tk)
      #define mode3 (mode2 + (2000 / test_tick))   //2 sec (4tk)

      #ifdef DEBUG
        Serial.print("test_lights\n");
      #endif 

      while(test_mode) { // test until we have ran out of time (ticks)

        if (tick_count < mode1){
          #ifdef DEBUG
            Serial.print("mode1\t");
            Serial.print(tick_count);
            Serial.print("\t");
            Serial.print(currentMillis);
            Serial.print("\n");
          #endif
          digitalWrite(ledTest, HIGH); 
          digitalWrite(ledRed1, HIGH);
          digitalWrite(ledAmr1, HIGH);
          digitalWrite(ledGrn1, HIGH);
          digitalWrite(ledRed2, HIGH);
          digitalWrite(ledAmr2, HIGH);
          digitalWrite(ledGrn2, HIGH);
        }
        else if (tick_count < mode2){
          #ifdef DEBUG
            Serial.print("mode2\t");
            Serial.print(tick_count);
            Serial.print("\t");
            Serial.print(currentMillis);
            Serial.print("\n");
          #endif
          digitalWrite(ledTest, !digitalRead(ledTest));
          if (tick_count == mode1){   // tk1
            digitalWrite(ledRed1, HIGH);
            digitalWrite(ledAmr1, LOW);
            digitalWrite(ledGrn1, LOW);
            digitalWrite(ledRed2, LOW);
            digitalWrite(ledAmr2, LOW);
            digitalWrite(ledGrn2, LOW);
          }
          else if (tick_count == (mode1 + 2)) {digitalWrite(ledRed1, LOW);  digitalWrite(ledAmr1, HIGH);}  // tk2
          else if (tick_count == (mode1 + 3)) {digitalWrite(ledAmr1, LOW);  digitalWrite(ledGrn1, HIGH);}  // tk3
          else if (tick_count == (mode1 + 4)) {digitalWrite(ledGrn1, LOW);  digitalWrite(ledRed2, HIGH);}  // tk4
          else if (tick_count == (mode1 + 5)) {digitalWrite(ledRed2, LOW);  digitalWrite(ledAmr2, HIGH);}  // tk5
          else if (tick_count == (mode1 + 6)) {digitalWrite(ledAmr2, LOW);  digitalWrite(ledGrn2, HIGH);}  // tk5
        }
        else if (tick_count < mode3){
          #ifdef DEBUG
            Serial.print("mode3\t");
            Serial.print(tick_count);
            Serial.print("\t");
            Serial.print(currentMillis);
            Serial.print("\n");
          #endif
          if (tick_count == mode2)  digitalWrite(ledGrn2, LOW);
          digitalWrite(ledTest, !digitalRead(ledTest));
          digitalWrite(ledRed1, !digitalRead(ledRed1));
          digitalWrite(ledAmr1, !digitalRead(ledAmr1));
          digitalWrite(ledGrn1, !digitalRead(ledGrn1));
          digitalWrite(ledRed2, !digitalRead(ledRed2));
          digitalWrite(ledAmr2, !digitalRead(ledAmr2));
          digitalWrite(ledGrn2, !digitalRead(ledGrn2));
        } 
        previousMillis = currentMillis; // wait till next tick
        while (currentMillis - previousMillis <= test_tick) __asm__("nop");
        tick_count += 1;  // knck the count over
        if (tick_count >= test_time) test_mode = false;   // Have we run out of ticks? (finished testing
      }
      #ifdef DEBUG
        Serial.print("test_mode finished\t");
        Serial.print(currentMillis);
        Serial.print("\n");
      #endif
      return;
}

bool tickLights() {
    /** \brief Traffic light ticker.
      * \param None
      * \return None
      * 
      * This function moves on both lights to the next stage.
      * ...
      */
      static int mode = 0;  /*!< Ticker for the mode */
      bool amb = false;   /*!< return if amber is on (short tick needed) */

      switch (mode) {
      case 0:   // RED (stop)
        analogWrite(ledRed1, redval);
        digitalWrite(ledAmr1, LOW);
        digitalWrite(ledGrn1, LOW);
        analogWrite(ledRed2, redval);
        digitalWrite(ledAmr2, LOW);
        digitalWrite(ledGrn2, LOW);
        mode += 1;
        break;
      case 1:   // AMBER/RED (ready go)
        analogWrite(ledRed1, redval);
        analogWrite(ledAmr1, ambval);
        digitalWrite(ledGrn1, LOW);
        analogWrite(ledRed2, redval);
        analogWrite(ledAmr2, ambval);
        digitalWrite(ledGrn2, LOW);
        amb = true;
        mode += 1;
        break;
      case 2:   // GREEN (go)
        digitalWrite(ledRed1, LOW);
        digitalWrite(ledAmr1, LOW);
        analogWrite(ledGrn1, grnval);
        digitalWrite(ledRed2, LOW);
        digitalWrite(ledAmr2, LOW);
        analogWrite(ledGrn2, grnval);
        mode += 1;
        break;
      case 3:   // AMBER (ready stop)
        digitalWrite(ledRed1, LOW);  // Set light1 on stop (red)
        analogWrite(ledAmr1, ambval);
        digitalWrite(ledGrn1, LOW);
        digitalWrite(ledRed2, LOW);
        analogWrite(ledAmr2, ambval);
        digitalWrite(ledGrn2, LOW);  // Set light2 on go (green)
        amb = true;
        mode = 0;
        break;
      default:
        mode = 0;
      }
  
  digitalWrite(ledTest, !digitalRead(ledTest)); // Toggle the test pin to indicate tick
  return amb;
}

void loop() {
    /** \brief Standard loop function.
      * \param None
      * \return None
      * 
      * This function runs the traffic lights.  
      * ...
      */

static uint8_t i = 0;
static uint8_t sendBuf = 0;
static uint8_t master_lr;
static uint8_t master_fb;

  #ifdef DEBUG
    Serial.print("loop\n");
  #endif

  if (fixCmdMode == modeRead && Serial.available()){    // Report on RC state (read pins)
    command = Serial.read();
    rcState[FWD] = !digitalRead(rcFwdPin);
    rcState[BWD] = !digitalRead(rcBwdPin);
    rcState[LFT] = !digitalRead(rcLftPin);
    rcState[RIT] = !digitalRead(rcRitPin); 
    
    for (i = 0; i < 4; i ++) sendBuf += (rcState[i] << i);  // send ACK as requested data in one byte
    Serial.write(sendBuf);
    Serial.flush();
    sendBuf = 0;
  }

  if (fixCmdMode == modeWrite && Serial.available()){    // Act on RC state (write pins)
    command = Serial.read();
    master_fb = (command & (mskFwd | mskBwd));
    master_lr = (command & (mskLft | mskRit));
    rcWrite(master_lr, master_fb);
  }
  
  if (fixCmdMode == 255){  // read data as debug info if no comm found to start
    rcState[FWD] = !digitalRead(rcFwdPin);
    rcState[BWD] = !digitalRead(rcBwdPin);
    rcState[LFT] = !digitalRead(rcLftPin);
    rcState[RIT] = !digitalRead(rcRitPin);
    #ifdef DEBUG
      for (i = 0; i < 4; i ++) Serial.print(rcState[i]);
    #endif
  }

  
  if (currentMillis - previousMillis >= interval) { // If timer tick
      previousMillis = currentMillis; // save the last time you blinked the LED
      amber_mode = tickLights();
      if (amber_mode) {interval = 2000;}    // make amber a shorter time   
      else {interval = interval_default;} 
  }
  
}

