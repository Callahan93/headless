"""
USB Arduino

This module is used to communicate via USB to an Arduino which processes the RC car driving data.
"""

import glob
import sys
import time
import serial
import numpy as np



class Arduino(object):
    """
        This is class Arduino.

        This class 'Arduino' is used to connect and communicate with an Arduino via USB.
    """

    ARD_SER_PARM = dict(
        # port=None,    # Not needed - define port at instant
        baudrate=9600,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=0.2
    )

    ARD_CMD_MODE2 = int('10000000', 2)
    ARD_CMD_FWD2 = int('00001000', 2)
    ARD_CMD_BWD2 = int('00000100', 2)
    ARD_CMD_LFT2 = int('00000010', 2)
    ARD_CMD_RIT2 = int('00000001', 2)

    ARD_CMD_MODE = 128
    ARD_CMD_FWD = 8
    ARD_CMD_BWD = 4
    ARD_CMD_LFT = 2
    ARD_CMD_RIT = 1

    ARD_READ = b'\x80'

    ARD_READ_ACK = b'\x7F'

    ARD_WRITE = b'\x00'
    ARD_WRITE_ACK = b'\xFF'

    BOOT_DELAY = 2.0

    def __init__(self):
        """
            The initiator for the Arduino class.

            This function is '__init__' and is used to initiate this class.
            It creates a serial object for communication but does not attempt to communicate.

            Args:
                self:           This function is part of the class 'Arduino'.

            Returns:
                An object.

            Examples:
                controller = Arduino()
        """

        self.myport = None      # Save the port I am on
        self.databuffer = []
        self.myser = serial.Serial(**Arduino.ARD_SER_PARM)       # Serial Object for communicating
        self.read_mode = True
        #self.serial_buffer = bytearray(b'')
        self.serial_byte = b''

        self.FWD = False
        self.BWD = False
        self.LFT = False
        self.RIT = False

        self.rc_state = np.zeros((4,), dtype=np.uint8)
        print('ARDUINO: Created!')

    @staticmethod
    def all_serial_ports():
        """Returns all open serial ports as a list

        Will check the environment your running on, and return a list of all available ports.
        Note that this function is never directly called.

        Returns:
            A list of all available ports e.g. COM1, COM11, COM12....

        Examples:
            ``>>> allports = all_serial_ports()``\n
            ``>>> print(allports, sep= ', ')``
        """

        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            # This excludes your current terminal "/dev/tty"
            ports = glob.glob('/dev/tty[A-Za-z]*')
        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')
        else:
            raise EnvironmentError('Unsupported platform')

        result = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                result.append(port)
            except (OSError, serial.SerialException):
                pass
        return result

    def find_arduino(self, write_mode=False):  # Will find the first arduino on the open serial ports
        """
            This will test all available serial ports to find the connected Arduino.

            A known ping and answer is used to find the Arduino
            , if multiple Arduinos are connected, only the first available one will be used.

            Args:
                self:           This function is part of the class 'Arduino'.

            Kwargs:
                write_mode:     Sets the Arduinos mode of operation. default: False

            Returns:
                Nothing, but leaves the correct serial port open for communication.

            Examples:
                slave = Arduino()
                slave.find_arduino()
        """

        print("ARDUINO: Finding Arduino...\nCurrently available serial ports:")
        ser_list = self.all_serial_ports()  # Grab the list of available ports
        print(ser_list)

        for each_port in reversed(ser_list):
            self.myser = serial.Serial(**Arduino.ARD_SER_PARM, port=each_port)  # Use standard serial settings
            time.sleep(Arduino.BOOT_DELAY)     # Required as Arduino resets on port open

            print((("ARDUINO: Trying {} ...").format(each_port)))
            if write_mode:
                self.myser.write(Arduino.ARD_WRITE)  # Send out the query
                self.read_mode = False
            else:
                self.myser.write(Arduino.ARD_READ)
                self.read_mode = True

                self.serial_byte = self.myser.read(1)  # Only ever sending 1 byte

            if (write_mode and self.serial_byte == Arduino.ARD_WRITE_ACK) or (self.serial_byte == Arduino.ARD_READ_ACK):
                self.myport = each_port  # Remember what port I am on
                print('ARDUINO: We found Arduino on: {}'.format(self.myport))
                print(self.serial_byte)
                #self.myser.close()     # leave port open to avoid resetting
                return True       # found first result
            else:
                self.myser.close()

        self.myser.close()
        return False

    def update_data(self):
        """
            This will retrive or send the latest information to the Arduino.

            Reading or writing is determined by 'self.read_mode'.

            Args:
                self:           This function is part of the class 'Arduino'.

            Returns:
                Nothing, but updates its internal state.

            Examples:
                while(current_time - previous_time) <= 20.0:    # do for 10 sec
                    current_time = time.time()
                    slave.update_data()
        """

        if not self.read_mode:
            return
        self.serial_byte = self.myser.read(1)
        my_int = int.from_bytes(self.serial_byte, byteorder='big')

        self.rc_state[3] = ((my_int & Arduino.ARD_CMD_FWD) >> 3)
        self.rc_state[2] = ((my_int & Arduino.ARD_CMD_BWD) >> 2)
        self.rc_state[1] = ((my_int & Arduino.ARD_CMD_LFT) >> 1)
        self.rc_state[0] = (my_int & Arduino.ARD_CMD_RIT)

        #print(self.rc_state)
        self.myser.write(Arduino.ARD_READ)  # send command ready for next read (assume we will back here very soon)

        return


def main_function():
    """
        This is the main function.

        This function is the main function (if using this file standalone).
        It finds the Arduino and prints its current status for 20 seconds before closing.
    """

    slave = Arduino()
    slave.find_arduino()

    current_time = time.time()
    previous_time = current_time
    while(current_time - previous_time) <= 20.0:    # do for 10 sec
        current_time = time.time()
        slave.update_data()

    slave.myser.close()     # remember to close port at end

    return


if __name__ == "__main__":
    print('Hello')
    print('This is \'check_usb.py\'\nSTART:')

    main_function()

    print('End Program')
    exit(0)
