"""
Camera Client

This module is used to operate as a client for recording of camera data (JPEG frames/video).
Threading is also used to ensure maximum processing speed. Use '-h' for usage.
"""

import io
import socket
import struct
import time
import picamera

# pylint: disable=R0903
# Too few public methods

class SplitFrames(object):
    """
        This is class SplitFrames.

        This class is 'SplitFrames' and is used to transmit (JPEG) images to a TCP server.
        This class is based on MJPEG video to achieve max frame-rate.
        The initial code/idea was taken from the [Jones, D (2016)]
        (https://picamera.readthedocs.io/en/release-1.13/recipes2.html#rapid-capture-and-streaming).
    """
    def __init__(self, cam_connection):
        """
            The initiator for the SplitFrames class.

            This function is '__init__' and is used to initiate this class.
            This accepts a connection object which will be used to send the images to.

            Args:
                self:               This function is part of the class 'SplitFrames'.
                cam_connection:     This is the connection object.

            Returns:
                An object.

            Examples:
                camera = ServCam(new_connection)
        """

        self.connection = cam_connection
        self.stream = io.BytesIO()
        self.count = 0

    def write(self, buf):
        """
            This function writes frames to the TCP socket.

            It uses a byte stream to packetise and send the data.
            If the data starts with 0xFF, 00xD8 then it prepends the length and sends the JPEG frame.

            Args:
                self:    This function is part of the class 'SplitFrames'.
                buf:     This data to be sent.

            Returns:
                None, will send data through the TCP socket.

            Examples:
                output = SplitFrames(connection)
                with picamera.PiCamera(resolution='VGA', framerate=30) as camera:
                time.sleep(2)
                camera.start_recording(output, format='mjpeg')  # record as video
                camera.wait_recording(record_time)   # Like time.sleep but will check for errors in background
                camera.stop_recording()     # record for 30 seconds
        """

        if buf.startswith(b'\xff\xd8'):  # Start of new frame; send the old one's length
            size = self.stream.tell()
           # print(size)
            if size > 0:    # Then the data
                self.connection.write(struct.pack('<L', size))  # Little-end Unsigned Long (4 bytes)
                self.connection.flush()     # Push it
                self.stream.seek(0)     # Reset stream
                self.connection.write(self.stream.read(size))   # Attach the image
                self.count += 1     # For info only (printing)
                self.stream.seek(0)
        self.stream.write(buf)  # Write it to the server


def main_function(rcrd_time=3, skt_timout=30.0, skt_prt=54321, skt_ip='192.168.0.100'):
    """
        This is the main function.

        This function is the main function (if using this file standalone).
        It accepts multiple arguments to allow the user to select the operating parameters.
        It will run a recording and send the frames to the server.

        Kwargs:
            rcrd_time:  The amount of time the camera should operate/record, will exit program when finsihed. default: 3 Sec
            skt_timout: The timout of the socket if no connection is made. default: 30.0
            skt_prt:    The port number to reach the socket on. default: 54321
            skt_ip:     The IPv4 address of the server. default: '192.168.0.100'

        Examples:
            >>> main_function(skt_ip=usr_ip, skt_prt=usr_prt, skt_timout=usr_to, rcrd_time=usr_rec)
    """

    client_socket = socket.socket()
    client_socket.connect((skt_ip, skt_prt))
    client_socket.settimeout(skt_timout)  # Timeout null connection at 30sec
    connection = client_socket.makefile('wb')   # Connection as a file like object
    print('Connected')
    # Manually send initial frame (desired frame rate)
    connection.write(struct.pack('<L', 4))  # Send length of data
    connection.write(struct.pack('<L', 30))  # Data to send (send desired frame rate)

    try:
        output = SplitFrames(connection)
        print('Recording...')
        with picamera.PiCamera(resolution='VGA', framerate=30) as camera:
            camera.framerate_delta = 1.9    # Minor adjustments to frame rate (allow for slight delay in processing at server)
            time.sleep(2)
            start = time.time()
            camera.vflip = True  # Correct the image
            camera.hflip = True
            camera.color_effects = (128, 128)   # Record/send as grayscale
            camera.start_recording(output, format='mjpeg')  # Record as video
            camera.wait_recording(rcrd_time)  # Like time.sleep but will check for errors in background
            camera.stop_recording()  # Record for 30 seconds then stop
            connection.write(struct.pack('<L', 0))  # Send the terminating 0-length to the socket for finishing
    finally:
        connection.close()
        client_socket.close()
        finish = time.time()
    print('Sent {} images in {} seconds at {}ffps'.format(output.count, finish - start, output.count / (finish - start)))
    return


if __name__ == "__main__":

    print('Hello')
    print('This is \'client_cam.py\'\nSTART:')
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-ip', '--ipAdd', nargs='?', type=str, help='IPv4 address of server', default='192.168.0.100')
    parser.add_argument('-prt', '--port', nargs='?', type=int, help='Port number of server', default=54321)
    parser.add_argument('-to', '--timeOut', nargs='?', type=float, help='Timeout of socket', default=30.0)
    parser.add_argument('-rec', '--recTime', nargs='?', type=int, help='Recording time', default=3)

    # Process args
    args = parser.parse_args()

    usr_ip = args.ipAdd
    usr_prt = args.port
    usr_to = args.timeOut
    usr_rec = args.recTime

    main_function(skt_ip=usr_ip, skt_prt=usr_prt, skt_timout=usr_to, rcrd_time=usr_rec)
    print('End Program')
    exit(0)
