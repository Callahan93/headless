"""
TCP Server

This module is used to operate as a server for recording of data (JPEG frames & RC car driving data).
It should be ran in the same folder as 'check_usb.py' as it uses this module to recive the RC car data via USB.
Threading is also used to ensure maximum processing speed. Use '-h' for usage.
"""

import time
import os
import socket
import struct
import threading as thrd
import glob
import numpy as np
import cv2
import check_usb as usb

# pylint: disable=R0902
# Too many instance attributes

lock = thrd.RLock()  # Global threading lock


class ServCam(object):
    """
        This is class ServCam.

        This class is 'ServCam' and is used to transmit (JPEG) images to a TCP server.
        This class is based on MJPEG video to achieve max frame-rate.
        The initial code/idea was taken from the [Jones, D (2016)]
        (https://picamera.readthedocs.io/en/release-1.13/recipes2.html#rapid-capture-and-streaming).
    """

    def __init__(self):
        """
            The initiator for the ServCam class.

            This function is '__init__' and is used to initiate this class.
            This is a blocking function (max 30 secs) and will wait till it receives a TCP connection.
            Initial threading flags are also set here as well as instance variables.

            Args:
                self:           This function is part of the class 'ServCam'.

            Returns:
                An object.

            Examples:
                cam1 = ServCam()
        """
        self.server_socket = socket.socket()
        self.server_socket.bind(('', 54321))  # Start a socket listening for any connections
        self.server_socket.settimeout(60.0)  # Timeout to 30sec
        self.server_socket.listen(1)  # Recive a max of 1 connections
        print('Starting up: on port: 54321')
        self.connection = self.server_socket.accept()[0].makefile('rb')  # Accept a single connection and make a file-like object out of it
        print('Connected')
        self.rec_string = ''    # Data coming in
        self.img_np = np.fromstring(self.rec_string)    # Will be used to convert the data to NP array (as OpenCV image)
        #self.img_to_show = self.img_np
        self.count_data = 0
        self.count_proc = 0      # Proc count is different as a diff thread is being used - makes it easier than locking/pausing just to count
        self.count_proc_shadow = 0

        # Set up the initial thread order
        self.ready_nxt_frm = thrd.Event()
        self.ready_nxt_frm.set()
        self.ready_pro_frm = thrd.Event()
        self.ready_pro_frm.clear()

        self.ready_post_pro_frm = thrd.Event()
        self.ready_post_pro_frm.set()

        self.end = False        # When True, threads need to exit cleanly
        self.target = 0     # Target FPS

        self.start_time = time.time()

    def get_img(self):
        """
            This will get an image from the TCP connection.

            This is a thread and hence operates ina while True loop until the connection is killed from the client
            or the end flag is set (currently not set until client closes connection). When ending it will close the connection.
            reads data (an image) based from the length byte and stores it as a string to a variable.

            Args:
                self:           This function is part of the class 'ServCam'.

            Returns:
                None. Will print when reciving data.

            Examples:
                thrd_data = thrd.Thread(target=cam1.get_img)    # Create the threads
                thrd_data.start()
                time.sleep(5)
                thrd_data.join()        # end the tasks
        """
        try:
            self.count_data = 0
            while not self.end:

                image_len = struct.unpack('<L', self.connection.read(struct.calcsize('<L')))[0]  # Read the 'length' byte as a 32-bit unsigned int
                if not image_len:  # If length is zero then quit
                    print('\nQuiting stream')
                    break
                if image_len < 10:      # Assume that this packet is not a image
                    self.target = int.from_bytes(self.connection.read(image_len), byteorder='little')
                    print('Target ffps: {}'.format(self.target))
                    #print('{}ffps'.format(int.from_bytes(self.connection.read(image_len), byteorder='little')))
                    continue

                # image_stream = io.BytesIO()     # Construct a stream to hold the image data and read the image data from the connection
                # image_stream.write(connection.read(image_len))
                # image_stream.seek(0)    # Rewind the stream, open it as an image with OpenCV

                self.count_data += 1
                #print('Received data {}\t'.format(self.count_data), end='')
                self.ready_nxt_frm.wait()  # Wait till flag is set (data has been processed)
                self.rec_string = self.connection.read(image_len)   # Read the data as a string
                self.ready_pro_frm.set()    # Data is ready to be processed
                self.ready_nxt_frm.clear()

        finally:
            self.connection.close()
            self.server_socket.close()
            print('Connection Closed')
            self.ready_pro_frm.set()    # Make sure the processing doesn't hang
            self.end = True     # End the threads

        return

    def pro_img(self, save_folder='bin/', save=False):
        """
            This will process an image from the instance variable (TCP connection).

            This is a thread and hence operates in a while True loop until the connection is killed from the client
            or the end flag is set.
            Reads the string as a numpy array (UINT8) via OpenCV to decode it into a workable variable.
            It also writes the frame to file

            Args:
                self:           This function is part of the class 'ServCam'.

            Returns:
                Saved image file. Will print when processing data.

            Examples:
                thrd_proc = thrd.Thread(target=cam1.pro_img)    # Create the threads
                thrd_proc.start()
                time.sleep(5)
                thrd_proc.join()        # end the tasks
        """
        self.count_proc = 0
        while not self.end:     # As this is a thread just loop here until we should end
            self.ready_pro_frm.wait()  # Wait till frame is ready to process
            with lock:
                #self.img_np = cv2.imdecode(np.fromstring(self.rec_string, np.uint8), cv2.IMREAD_COLOR)
                self.img_np = cv2.imdecode(np.fromstring(self.rec_string, np.uint8), cv2.IMREAD_GRAYSCALE)
                self.img_np = cv2.GaussianBlur(self.img_np, (3, 3), 0)      # Blur to remove noise
                #self.img_np = cv2.medianBlur(self.img_np, 5)
                #self.img_np = cv2.bilateralFilter(self.img_np, 5, 40, 40)
            self.ready_pro_frm.clear()
            self.ready_nxt_frm.set()    # Image is read for processing so read another frame

            self.count_proc += 1  # Independent count (should never be more than one off the data count)
            with lock:
                self.count_proc_shadow += 1
            if save:
                cv2.imwrite('{}{}.jpg'.format(save_folder, self.count_proc), self.img_np)  # Write frame to file


            #print('Processed Image {}'.format(self.count_proc))
        print('Ending Processing')
        return

    def pro_nn_data(self, save_np_folder='./', save=True, dead=False, combine=False):
        """
            This will process the driving data (forward/backward/left/right) from the connected Arduino with the input image.

            This is a thread and hence operates in a while True loop until the connection is killed from the client
            or the end flag is set.
            The first operation is to find and connect to the Arduino (using the 'check_usb.py' module)
            before repeatedly making calls to determine the current status. This information is tied with the closest image frame.
            The image frame is cropped and modified with lane detection ('detect_lane' function).
            All this information is stored as NumPy arrays and saved to file.

            Args:
                self:           This function is part of the class 'ServCam'.

            Kwargs:
                save_np_folder: Define the folder location to save the neural network data. default: './'
                save:           If true then it will save the recorded data. default: True
                dead:           If true then this function will immediately return with no processing. default: False
                combine:        If true then this combine any existing data in the folder with the new recording. default: False

            Returns:
                Nothing, any saved data is recorded and saved in the folder specified.

            Examples:
                thrd_nn = thrd.Thread(target=cam1.pro_nn_data, kwargs={'save_np_folder': './', 'dead': False})
                print('Booting Threads...')
                thrd_nn.start()
        """

        if dead:    # If we dont want this process, this is dead so just clear flags to avoid other threads hanging
            return

        #nn_ins = np.zeros((1, 9600), dtype=np.float32)   # 32float gives full res from 16int
        nn_ins = np.zeros((1, 2400), dtype=np.float32)  # 32float gives full res from 16int
        nn_outs = np.zeros((1, 4), dtype=np.float32)     # float (use as confidence)
        nn_ins_xst = np.zeros((1, 2400), dtype=np.float32)
        nn_outs_xst = np.zeros((1, 4), dtype=np.float32)

        rc_remote = usb.Arduino()
        if rc_remote.find_arduino():
            rc_remote.update_data()
        else:
            print('Arduino not found!')
            return

        cur_frm = 0
        prev_frm = 0

        self.ready_pro_frm.wait()   # Wait for initial boot up
        while not self.end:
            while cur_frm <= prev_frm:  # Wait till the new frame is ready
                time.sleep(0.02)  # Avoid hogging of the lock and skip some frames (too similar)
                with lock:
            #        full_img = self.img_np  # grab frame
                    cur_frm = self.count_proc_shadow
            prev_frm = cur_frm
            #time.sleep(0.03)    # Wait at least one frame as the thread is not clearing the flag
            #self.ready_pro_frm.wait()   # Wait till next frame is ready
            with lock:
                full_img = self.img_np  # Grab frame

            roi = full_img[240:480, :]      # Crop array to roi [y(h), x(w)] from 480x640 (bottom half)
            lane_vision = self.detect_lane(roi)   # Process lanes and resize here, returns new image

            # If there are no illegal states
            if not (rc_remote.rc_state[3] & rc_remote.rc_state[2]) and not (rc_remote.rc_state[1] & rc_remote.rc_state[0]) and (max(rc_remote.rc_state) != 0):
                if np.count_nonzero(rc_remote.rc_state) > 0:
                    #nn_ins = np.vstack((nn_ins, lane_vision.reshape(1, 9600).astype(np.float32)))
                    nn_ins = np.vstack((nn_ins, lane_vision.reshape(1, 2400).astype(np.float32)))
                    nn_outs = np.vstack((nn_outs, rc_remote.rc_state.astype(np.float32)))
                    print(rc_remote.rc_state)
            else:
                print('Illegal RC Command {}'.format(cur_frm))

            rc_remote.update_data()     # This can take time (up to 0.1 sec)
            #print('Laned Frame {}'.format(cur_frm))

        rc_remote.myser.close()  # Remember to close port at end
        with lock:
            print('Caught {}/{} frames at {} fps for NN'
                  .format(nn_ins.shape[0] - 1, self.count_proc, nn_ins.shape[0] / (int(time.time()) - self.start_time)))
            print((time.time() - self.start_time))

        if combine:
            training_data = glob.glob('./np_data*.npz')  # Get all npz files
            for single_npz in training_data:
                with np.load(single_npz) as data:
                    print(data.files)
                    nn_ins_temp = data['nn_ins']
                    nn_outs_temp = data['nn_outs']
                    print(nn_ins_temp.shape)
                    print(nn_outs_temp.shape)
                    nn_ins_xst = np.vstack((nn_ins_xst, nn_ins_temp))  # Stack all the existing files
                    nn_outs_xst = np.vstack((nn_outs_xst, nn_outs_temp))

        if save:
            nn_ins = nn_ins[1:, :]  # Delete the starting zeros
            nn_outs = nn_outs[1:, :]
            nn_ins_xst = nn_ins_xst[1:, :]
            nn_outs_xst = nn_outs_xst[1:, :]

            nn_ins = np.vstack((nn_ins, nn_ins_xst))   # Stack on top of existing file
            nn_outs = np.vstack((nn_outs, nn_outs_xst))

            print('Saving npz data...', end='')
            if combine:
                np.savez('{}comb_np_data{}.npz'.format(save_np_folder, int(round(time.time()))), nn_ins=nn_ins, nn_outs=nn_outs)  # Save data as numpy file
            else:
                np.savez('{}np_data{}.npz'.format(save_np_folder, int(round(time.time()))), nn_ins=nn_ins, nn_outs=nn_outs)   # Save data as numpy file
            print('Saved')
            print(nn_outs, nn_ins)   # For sanity
            print(nn_ins.shape, nn_outs.shape)

        return

    @staticmethod
    def detect_lane(in_img, origin=False):
        """
            This will process the lane detction on a given image.

            The image is first scaled down by a factor of 4 before using, blurring, thresholding and Hough lines to detect the lanes.
            It will then draw the detected lanes on the image before being returned.

            Args:
                in_img:     Input image to process.

            Kwargs:
                origin:     If false then a blank image is used to draw the lanes, else the original is used. default: False

            Returns:
                the image with lanes drawn on.

            Examples:
                lane_vision = self.detect_lane(roi)
        """

        in_img = cv2.resize(in_img[120:240, :], (0, 0), fx=0.25, fy=0.25, interpolation=cv2.INTER_AREA)  # reduce size for ANN (/4) with cropped bottom half only

        orig = in_img
        nn_img = np.full((30, 80), 255, dtype=np.uint8)     # make a white image

        in_img = cv2.medianBlur(in_img, 3)
        in_img = cv2.adaptiveThreshold(in_img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 13, 7)
        in_img = cv2.bitwise_not(in_img)    # If no canny then must do this (lines should be white)

        linesP = cv2.HoughLinesP(in_img, 1, (1 * np.pi) / 180, 20, None, 8, 5)

        if not origin:
            if linesP is not None:
                for i in range(0, len(linesP)):
                    l = linesP[i][0]
                    cv2.line(nn_img, (l[0], l[1]), (l[2], l[3]), (0, 0, 0), 2, cv2.LINE_AA)  # Draw lines
            return nn_img
        else:
            if linesP is not None:
                for i in range(0, len(linesP)):
                    l = linesP[i][0]
                    cv2.line(orig, (l[0], l[1]), (l[2], l[3]), (0, 0, 0), 2, cv2.LINE_AA)  # Draw lines on original
            return orig

    def show_img(self):
        """
            This shows the received image to the monitor.

            This is a thread and hence operates in a while True loop until the connection is killed from the client
            or the end flag is set.
            Uses the OpenCV imgShow() command to put the image on the display.
            As this is the slowest task it will only show the latest image on every loop so some frames might be dropped/not shown.
            NOTE: This must be ran as part of the main thread as it involves graphics

            Args:
                self:           This function is part of the class 'ServCam'.

            Returns:
                None. Will show image on monitor during stream.

            Examples:
                while not cam1.end:     # Run this program as the main thread
                    cam1.show_img()     # This needs to be in the main thread as its graphics
                thrd_data.join()        # end the tasks
        """

        self.ready_pro_frm.wait()
        self.ready_nxt_frm.wait()
        self.start_time = time.time()
        cv2.namedWindow('Stream', cv2.WINDOW_NORMAL)     # Make a window to view the frams and allow it to be resized)

        while not self.end:     # This is running in the main thread
            with lock:
                img_to_show = self.img_np
            cv2.imshow('Stream', img_to_show)
            cv2.waitKey(10)  # Required to actually process and show image
            cv2.destroyAllWindows()  # Clean up windows
        cv2.destroyAllWindows()     # Clean up windows
        return


def main_function(the_folder='./bin/', the_save=False):
    """
        This is the main function.

        This function is the main function (if using this file standalone).
        It runs a TCP server to accept and then process images received from a client camera.
        Note that this program realise on threads

        Kwargs:
            the_folder: The folder to save the frames to (if 'the_save' is True)
            the_save:   Set to True if you want to save every frame

    """

    if the_save:
        if not os.path.exists(the_folder):  # Does the folder "small" exists ?
            print('Creating directory {}'.format(the_folder))
            os.makedirs(the_folder)  # If not, create it for our results
        elif os.listdir(the_folder):  # If folder exists and is not empty
            delete_files = [f for f in os.listdir(the_folder)]  # List all files in folder
            for each in delete_files:
                os.remove('{}{}'.format(the_folder, each))
                print('.', end='')
            print('Cleaned : {}'.format(the_folder))

    cam1 = ServCam()       # Make instance
    thrd_data = thrd.Thread(target=cam1.get_img)    # Create the threads
    thrd_proc = thrd.Thread(target=cam1.pro_img, kwargs={'save_folder': the_folder, 'save': the_save})
    thrd_nn = thrd.Thread(target=cam1.pro_nn_data, kwargs={'save_np_folder': './', 'dead': False})

    print('Booting Threads...')
    thrd_nn.start()
    thrd_data.start()
    thrd_proc.start()

    while not cam1.end:     # Run this program as the main thread
        cam1.show_img()     # This needs to be in the main thread as its graphics
    thrd_data.join()        # End the tasks
    thrd_proc.join()
    thrd_nn.join()

    return


if __name__ == "__main__":
    print('Hello')
    print('This is \'server.py\'\nSTART:')

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-sv', '--save', nargs='?', type=str, help='pass yes if you want to save the frames', default='n')
    parser.add_argument('-fld', '--folder', nargs='?', type=str, help='the save location of the frames', default='bin/')

    # Process args
    args = parser.parse_args()

    usr_save = args.save
    usr_folder = args.folder
    if usr_save == 'n':
        main_function(the_folder=usr_folder, the_save=False)
    else:
        main_function(the_folder=usr_folder, the_save=True)

    print('End Program')
    exit(0)
    """
    
    this = cv2.imread('../classifier/bin/n5_000244.jpg', 0)
    cv2.imwrite('./0_original.jpg', this)
    this = this[120:240, :]
    cv2.imwrite('./1_cropped.jpg', this)
    #cv2.imshow('Stream', new)
    #cv2.waitKey(5000)  # Required to actually process and show image
    #exit(0)
    resized_img = cv2.resize(this, (80, 30), interpolation=cv2.INTER_AREA)  # resize to 1/4 of the BH of the image
    this = resized_img

    empty = np.zeros((120, 320), dtype=np.uint8)
    #empty = np.zeros((30, 80), dtype=np.uint8)

    this = cv2.medianBlur(this, 3)
    cv2.imwrite('./2_median_blur.jpg', this)
    ret, bin = cv2.threshold(this, 100, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    th3 = cv2.adaptiveThreshold(this, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 13, 7)


    v = np.median(resized_img)
    lower = int(max(0, (1.0 - 0.33) * v))
    upper = int(min(255, (1.0 + 0.33) * v))
    edged = cv2.Canny(th3, lower, upper)
    cv2.imwrite('./4_canny.jpg', edged)

    cdst = cv2.cvtColor(edged, cv2.COLOR_GRAY2BGR)
    #cdstP = np.copy(cdst)
    cdstP = this
    linesP = cv2.HoughLinesP(edged, 1, (1 * np.pi) / 180, 20, None, 8, 5)
    if linesP is not None:
        for i in range(0, len(linesP)):
            l = linesP[i][0]
            cv2.line(empty, (l[0], l[1]), (l[2], l[3]), (255, 255, 255), 2, cv2.LINE_AA)

    cv2.imshow('StreamHough', empty)
    cv2.imwrite('./5_lanes.jpg', empty)
    cv2.imshow('Stream0', th3)
    cv2.imwrite('./3_adaptive_thresh.jpg', th3)
    cv2.imshow('Stream', bin)
    cv2.imwrite('./3b_binary_thresh.jpg', bin)
    cv2.waitKey(10000)  # Required to actually process and show image
    cv2.destroyAllWindows()
    exit(0)  #temp test
    """
