"""
Test Face Detection

This module is used to test the basic operation of the OpenCV face detection.
The code is modified from [OpenCV (2017)]
(https://docs.opencv.org/3.3.0/d7/d8b/tutorial_py_face_detection.html).
It uses the defualt OpenCV Haar cascade for face detection to draw a box around any found faces in the video stream.
"""

# pylint: disable=W0611
# unused-import

import time
from picamera.array import PiRGBArray
from picamera import PiCamera
import cv2
import numpy as np


# Initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()     ## The camera class object
camera.resolution = (640, 480)
camera.framerate = 10
rawCapture = PiRGBArray(camera, size=(640, 480))

# Import the cascade file
face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

# Allow the camera to warmup
time.sleep(0.1)
 
# Capture frames from the camera
for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
    # grab the raw NumPy array representing the image, then initialize the timestamp
    img = frame.array

    # Convert to grayscale
    gray = cv2.cvtColor(frame.array, cv2.COLOR_BGR2GRAY)

    # Detect faces
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)

    # Draw boxes around detections
    for (x, y, w, h) in faces:
        cv2.rectangle(frame.array, (x, y), (x+w, y+h), (255, 0, 0), 2)
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = frame.array[y:y+h, x:x+w]

    # Show the modified frame
    cv2.imshow('image', img)
    key = cv2.waitKey(1) & 0xFF

    # Clear the stream in preparation for the next frame
    rawCapture.truncate(0)

    # If the `q` key was pressed, break from the loop
    if key == ord("q"):
        break
