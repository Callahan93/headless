"""
Test Camera Image

This module is used to test the basic operation of the Raspberri Pi camera (PiCam).
The code is taken from [Rosebrock, A (2015)]
(https://www.pyimagesearch.com/2015/03/30/accessing-the-raspberry-pi-camera-with-opencv-and-python/).
It captures and displays a single frame for 10 seconds.
"""

import time
from picamera.array import PiRGBArray
from picamera import PiCamera
import cv2

# Init camera and grab ref to raw capture
camera = PiCamera()
rawCapture = PiRGBArray(camera)

# Allow the cam to warm up
time.sleep(0.1)

# Grab an image
camera.capture(rawCapture, format="bgr")
image = rawCapture.array

# Display result and wait for keypress
cv2.imshow("Image", image)
cv2.waitKey(10)

exit(0)
