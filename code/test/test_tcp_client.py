"""
Test TCP Client

This module is used to test the basic operation of the TCP client using the local host.
It will send a 'Hello World' message once connected to the server.
"""

import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)    # TCP/IP socket
sock.settimeout(30.0)   # Timeout to 30sec
server_address = ('localhost', 54321)
#server_address = ('192.168.4.2', 54321)   # Assume server connected first at 192.168.4.2
print('Connecting to: {} port: {}'.format(*server_address))
sock.connect(server_address)


try:
    message = b'Hello World.'
    print('Sending {}'.format(message))
    sock.sendall(message)   # Send data

    amount_received = 0
    amount_expected = len(message)
    while amount_received < amount_expected:    # Look for the response
        data = sock.recv(64)
        amount_received += len(data)
        print('Received {}'.format(data))

finally:
    print('Closing socket')
    sock.close()
    exit(0)
