"""
Test TCP Server

This module is used to test the basic operation of the TCP server using the local host.
It will print any message received to the console.
"""

import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)    # TCP/IP socket
sock.settimeout(30.0)   # timeout to 30sec
server_address = ('', 54321)    # Listen for connection from any IPv4 address
print('Starting up: {} port: {}'.format(*server_address))
sock.bind(server_address)   # Bind the socket to an public port

sock.listen(1)  # Listen for max of one incoming connection
flag = True
while flag:
    print('Waiting for a connection...')
    connection, client_address = sock.accept()
    try:
        print('Connection from {}'.format(client_address))

        while True:     # Receive the data in small chunks and retransmit it
            data = connection.recv(64)  # buffer size
            print('received {}'.format(data))
            if data:
                print('sending data back to the client')
                connection.sendall(data)
            else:
                print('no data from', client_address)
                break

    finally:
        connection.close()  # Clean up the connection
        flag = False
exit(0)
