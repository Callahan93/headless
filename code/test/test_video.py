"""
Test Camera Video

This module is used to test the basic operation of the Raspberry Pi camera (PiCam).
The code is modified from [Rosebrock, A (2015)]
(https://www.pyimagesearch.com/2015/03/30/accessing-the-raspberry-pi-camera-with-opencv-and-python/).
It uses OpenCV to display a video stream therefor proving a working OpenCV and camera installation.
"""

import time
from picamera.array import PiRGBArray
from picamera import PiCamera
import cv2

# Initialise the camera and grab a reference to the raw camera capture
camera = PiCamera()     ## The camera class object
camera.resolution = (640, 480)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(640, 480))

# Allow the camera to warm-up
time.sleep(0.1)

# Capture frames from the camera
for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):

    # Grab the raw NumPy array representing the image, then initialize the timestamp
    image = frame.array

    # Show the frame
    cv2.imshow("Frame", image)
    key = cv2.waitKey(1) & 0xFF     # 0xFF key is 'q'

    # Clear the stream in preparation for the next frame
    rawCapture.truncate(0)

    # If the `q` key was pressed, break from the loop
    if key == ord("q"):
        break
