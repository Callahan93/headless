## Common Commands  

# [Git LFS](https://git-lfs.github.com/).

`git lfs install` Install git LFS.

`git lfs track "*.psd"` Track specific extensions.

Also check `.gitattributes` show lfs filters.

`git lfs ls-files` Shows all files being tracked by LFS.

[Other Git LFS link](https://github.com/git-lfs/git-lfs/wiki/Tutorial)
