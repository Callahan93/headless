# Logo

\image html 4773687.png
\image latex 4773687.eps "headless - 4773687" width=10cm

This logo is generated via this [identicon generator](http://identicon.net/) with the below settings and the data used to generate this identicon is the project ID form GitLab..

Size: 210  

Rounds: 1  

Salts: 0 
 
Base: HEX 
 
Hash: SHA-512  

Data: 4773687

To ensure images are properly inserted into [Doxygen](http://www.stack.nl/~dimitri/doxygen/index.html), the [image command](https://www.star.bnl.gov/public/comp/sofi/doxygen/commands.html#cmdimage) must be used.