## Useful Links  

[Doxygen Manual](https://www.stack.nl/~dimitri/doxygen/manual/index.htm).

[GitLab YAML features](https://docs.gitlab.com/ee/ci/yaml/).

[VIM editor commands](http://www.radford.edu/~mhtay/CPSC120/VIM_Editor_Commands.htm) as used by Git CLI.
Use insert `i` then edit the file, then use `ESC` then save and quite with `:wq`.

[Alpine Linux packages](https://pkgs.alpinelinux.org/packages) searching - make sure they are stable first! Can use wildcard search `*`.

[Pylint manual](https://pylint.readthedocs.io/en/latest/user_guide/).

[Useful RaspberryPi command](http://www.circuitbasics.com/useful-raspberry-pi-commands/). 

[OpenCV 3.3.0 manual](https://docs.opencv.org/3.3.0/index.html)
